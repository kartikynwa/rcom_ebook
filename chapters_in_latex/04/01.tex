\section{Detailed Analysis of Living Standards Under Mao Zedong,
With Academic Sources}

\emph{\href{https://www.reddit.com/r/communism/comments/c8zxob/detailed_analysis_of_living_standards_under_mao/}{Link
to post} by /u/flesh\_eating\_turtle}

\subsection*{Introduction}

Most people (including many who call themselves socialists) have a very
deceptive impression of Mao Zedong. They tend to rely on bourgeois myths
and fictions as their only sources of information about him, and they
thus lack a proper understanding of his immense achievements (which are
ignored), as well as his flaws (which are exaggerated and
mischaracterized).

Because Mao's ideology continues to be the driving force behind the most
active and revolutionary sector of the international communist movement
(as demonstrated by the Naxalites in India, the NPA in the Philippines,
and many others), it is important that we have a correct understanding
of Maoist policies, and the immense gains they made for the Chinese
people.

\subsection*{General Overview of Living Standards}

Our primary source in this section will be an in-depth study conducted
by Amartya Sen, Professor of Economics at Harvard University, and fellow
of Trinity College at Cambridge University. Sen was awarded the Nobel
Memorial Prize in Economic Sciences for his work comparing living
standards in the People's Republic of China (particularly during the
Maoist period) to those in India:

\begin{itemize}
\tightlist
\item
  \href{https://books.google.com/books?id=IZUPXjnGnhEC\&printsec=frontcover\&dq=inauthor:\%22Amartya+Kumar+Sen\%22+china+india+famine\&hl=en\&sa=X\&ved=0ahUKEwjGsY2Lx5fjAhVjdt8KHTRIDSoQ6AEIKjAA\#v=onepage\&q\&f=false}{Harvard
  University \textbar{} Perspectives on the Economic and Human
  Development of India and China}
\end{itemize}

The results of the study can be summarized by the following remark, in
which Sen discusses China's decidedly superior achievements, and
attributes them directly to the socialist ideology of the Maoist period:

\begin{quote}
Because of its radical commitment to the elimination of poverty and to
improving living conditions - a commitment in which Maoist as well as
Marxist ideas and ideals played an important part - China did achieve
many things that the Indian leadership failed to press for and pursue
with any vigor. The elimination of widespread hunger, illiteracy, and
ill health falls solidly in this category. When state action operates in
the right direction, the results can be quite remarkable, as is
illustrated by the social achievements of the pre-reform {[}Maoist{]}
period.
\end{quote}

Another important comment summarizing the findings of the study is as
follows:

\begin{quote}
We argue, in particular, that the accomplishments relating to education,
healthcare, land reforms, and social change in the pre-reform
{[}Maoist{]} period made significantly positive contributions to the
achievements of the post-reform period. This is so not only in terms of
their role in sustained high life expectancy and related achievements,
but also in providing firm support for economic expansion based on
market reforms.
\end{quote}

Sen states here that the Maoist period saw enormous increases in quality
of life for the Chinese people, as well as important economic
developments, without which the economic expansion following the 1979
market reforms most likely could not have taken place.

Sen notes that during the Maoist period, a "remarkable reduction in
chronic undernourishment took place," attributing this to the socialist
policies implemented by Mao's government:

\begin{quote}
The casual processes through which the reduction of undernourishment was
achieved involved extensive state action including redistributive
policies, nutritional support, and of course health care (since
undernourishment is frequently caused by parasitic diseases and other
illnesses).
\end{quote}

Sen focuses more attention on the remarkable advances in healthcare
during the Maoist period:

\begin{quote}
China's achievements in the field of health during the pre-reform period
include a dramatic reduction of infant and child mortality and a
remarkable expansion of longevity.
\end{quote}

It is also noted that China's life expectancy approximately
\emph{doubled} during the Maoist period, from approx. 35 years in 1949,
to 68 years in 1981 (when Dengist reforms began to take effect). This is
further elaborated on in the next source.

On the issue of education, Sen notes that the huge improvements
(including dramatic increases in literacy) can be attributed primary to
the pre-reform Maoist period:

\begin{quote}
China's breakthrough in the field of elementary education had already
taken place before the process of economic reform was initiated at the
end of the seventies. Census data indicate, for instance, that literacy
rates in 1982 for the 15-19 age group were already as high as 96 percent
for males and 85 percent for females.
\end{quote}

Let us examine the issue of public health in more detail.

\subsection*{Further Research on Public Health and Life Expectancy}

Another excellent source on public health in Maoist China comes from the
journal \emph{Population Studies}, in a study conducted by researchers
from Stanford University and the National Bureau for Economic Research:

\begin{itemize}
\tightlist
\item
  \href{https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4331212/}{Population
  Studies \textbar{} An Exploration of China's Mortality Decline Under
  Mao: A Provincial Analysis, 1950-1980}
\end{itemize}

One important comment is as follows

\begin{quote}
China's growth in life expectancy at birth from 35--40 years in 1949 to
65.5 years in 1980 is among the most rapid sustained increases in
documented global history.
\end{quote}

This alone goes to show the massive benefits attained by the socialist
policies under Mao Zedong. More important information is provided in the
study, dealing with hospital and medical resources:

\begin{quote}
Physician and hospital supply grew dramatically under Mao due to a
variety of factors (including increases in government financing, the
introduction of social insurance for urban public employees, and the
launch of China's Rural Cooperative Medical System in the mid-1950's).
Rural Cooperative Medical Schemes (CMS) were vigorously promoted and
became widespread in the late 1960's as part of the Cultural Revolution.
\end{quote}

The study confirms Sen's analysis of education:

\begin{quote}
China made large strides in primary and secondary education under Mao.
\end{quote}

It also quotes other research which found that the rapid gains in
Chinese healthcare can be attributed to the specific socialist policies
implemented:

\begin{quote}
China's mortality decline between 1953 and 1957, which resembles that of
the US between 1900 and 1930, was ``primarily due to the unique social
organisation of Chinese public health practices.''
\end{quote}

Note that China achieved in four years what the United States took
thirty years to accomplish, due to their differing systems (i.e.
socialism vs. capitalism). The study also confirms the immense success
of Maoist vaccination programs:

\begin{quote}
Systematic efforts to vaccinate the population against polio, measles,
diphtheria, whooping cough, scarlet fever, and cholera were rapid and
reputedly successful (China nearly eradicated smallpox within the span
of only three years, with the last documented cases occurring in Tibet
and Yunnan in 1960).
\end{quote}

Additional citations for the claims in the above quotes are provided in
the original study.

\subsection*{Analysis of the Great Chinese Famine - Comparison to
Capitalist India}

In analyzing this topic, we may look to another work by Amartya Sen, his
book \emph{Hunger and Public Action,} written with John Dreze:

\begin{itemize}
\tightlist
\item
  \href{https://scholar.harvard.edu/sen/publications/hunger-and-public-action}{Harvard
  University \textbar{} Hunger and Public Action}
\end{itemize}

Sen and Dreze point out that, while the Chinese famine was devastating,
it pales in comparison to the ordinary mortality rates which occur under
capitalism in an otherwise comparable nation like India:

\begin{quote}
...it is important to note that despite the gigantic size of excess
mortality in the Chinese famine, the extra mortality in India from
regular deprivation in normal times vastly overshadows the former.
Comparing India's death rate of 12 per thousand with China's of 7 per
thousand, and applying that difference to the Indian population of 781
million in 1986, we get an estimate of excess normal mortality in India
of 3.9 million per year. This implies that every eight years or so more
people die in India because of its higher regular death rate than died
in China in the gigantic famine of 1958 -- 61. India seems to manage to
fill its cupboard with more skeletons every eight years than China put
there in its years of shame.
\end{quote}

This comes out to more than \emph{100 million} excess deaths in India
alone from 1947 (when India become independent) to 1980. As Paul
Heideman put it, writing in \emph{Jacobin}:

\begin{quote}
In other words, though India experienced no concentrated period of
starvation which can be easily identified and hung around the neck of a
particular ideology, its ordinary conditions for the latter half of the
twentieth century, in which an extraordinarily unequal distribution of
land obtained, created an excess mortality that, over the long term,
dwarfed that of the worst famine of the century.
\end{quote}

This demonstrates the effects that capitalism has on a developing
nation. This is all the more shocking when compared to the immense gains
made in the People's Republic of China, described in the earlier
sections of this discussion.

\subsection*{Conclusion}

The People's Republic of China under Mao Zedong made enormous strides in
living standards, dramatically bettering the lives of hundreds of
millions of people. While the bourgeois establishment continues to
misrepresent and distort the legacy of Mao Zedong, he remains an
inspirational figure to the billions of people around the world who have
benefited, either directly (via the improvements mentioned above) or
indirectly (via his influence on global revolutionary movements), from
his work.
