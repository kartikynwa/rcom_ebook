\section{The Transformation Problem}

\emph{\href{https://www.reddit.com/r/communism/comments/2f16jo/transformation_problem_discussion_post/}{Link
to post} by /u/MasCapital}

\subsection*{Transformation Problem Discussion Post}

In this post I will discuss what the "Transformation Problem" is, why it
is important, and a few solutions/responses to it. I also want this to
be a discussion thread, so ask any questions you'd like, object to
anything I wrote, etc.

\subsection*{Why?}

I will actually start with why the Transformation Problem is important,
before saying what it is. This will give you some motivation to learn
more about it, and will tell you why you should keep reading this post,
if it gets boring or confusing.

It is very important to understand the Transformation Problem because it
alone, especially among academics, is the single most common reason
Marx's value theory is rejected. It is argued that Marx's own solution
to this problem fails because it is inconsistent. It is also argued that
the problem \emph{cannot} be solved in a way that is consistent with
other central claims of Marx's theory. It is therefore incumbent upon
all Marxists to understand and take a position on this issue.

Marx's value theory is the basis of scientific socialism as opposed to
utopian socialism. You don't need any scientific theory to feel moral
outrage at the consequences of capitalism. Socialists were doing that
long before Marx and concocting morally ideal societies in response. But
they didn't have an adequate scientific theory that explained why
capitalism produces those consequences and its place in the historical
development of society. The distinction between utopian and scientific
socialism is analogous to pre-scientific and scientific medicine. Before
the advent of scientific medicine, people's methods of removing ailments
were ineffective. Similarly, successfully moving past capitalism
requires understanding capitalism and such an understanding is provided
by Marx. If Marx's critics are right, then his failure to solve the
transformation problem crumbles the foundation of scientific socialism.

\subsection*{What (and What Not!)}

A \emph{key} insight into understanding the Transformation Problem is
understanding what it is \emph{not}. The fuller name for the problem
comes from the title of chapter 9 of {[}\emph{Capital, Vol. 3}{]}
(\url{http://digamo.free.fr/penguin3.pdf}), The Transformation of
Commodity Values into Prices of Production. The follwing is
\emph{extremely} important: \emph{The Transformation Problem, the
problem of transforming values into prices of production, is not the
problem of transforming one kind of thing, abstract labor or its
duration measured in labor-time (e.g. hours), into another kind of
thing, money measured in the money commodity (e.g. ounces of gold or
bills signifying gold).} The move from labor-time units to money units
is given by the "monetary expression of labor time" or MELT and has
nothing directly to do with the Transformation Problem. In an economy
with a money commodity, the MELT is simply a unit of money commodity
divided by the average labor time required to produce it. (I won't
address the case of economies without a money commodity. We can discuss
this in the comments if you'd like.) For example, if it takes on average
1 hour to produce 1 ounce of gold, then the MELT is 1 ounce of gold per
hour. If a dollar bill signifies an ounce of gold, then the MELT is
\$1/hr. The average hour of productive labor then creates \$1 of value.
This allows you to convert back and forth between labor time and money.

The Transformation Problem is not a problem of transforming units of
measurement, of going from labor-time to money. If you thought this, and
it is a very common misunderstanding, you have fundamentally
misunderstood Marx's value theory. I will go so far as to say that
understanding why this is wrong will help clarify the entirety of Marx's
value theory. Prices of production are simply prices which bring the
capitalist the average rate of profit. Marx's Transformation Problem is
the same problem on which Smith, Ricardo, and all classical political
economy floundered: How is the existence of a general rate of profit
consistent with the law of (surplus) value, the determination of value
by labour-time? In \emph{Theories of Surplus Value} Marx is clear that
this is the central problem that he needs to solve:

\begin{quote}
\href{https://www.marxists.org/archive/marx/works/1863/theories-surplus-value/ch03.htm\#s5}{The
seven times greater profit in the one manufactory as compared with the
other - or in general the law of profit, that it is in proportion to the
magnitude of the capital advanced - thus prima facie contradicts the law
of surplus-value or of profit (since Adam Smith treats the two as
identical) that it consists purely of the unpaid surplus-labour of the
workmen. Adam Smith puts this down with quite naïve thoughtlessness,
without the faintest suspicion of the contradiction it presents. All his
disciples - since none of them considers surplus-value in general, as
distinct from its determinate forms - followed him faithfully in this.
With Ricardo, as already noted, it merely comes out even more
strikingly.} {[}...{]}
\end{quote}

\begin{quote}
\href{https://www.marxists.org/archive/marx/works/1863/theories-surplus-value/ch10.htm\#sa4}{Instead
of postulating this general rate of profit, Ricardo should rather have
examined in how far its existence is in fact consistent with the
determination of value by labour-time, and he would have found that
instead of being consistent with it, prima facie, it contradicts it, and
that its existence would therefore have to be explained through a number
of intermediary stages, a procedure which is very different from merely
including it under the law of value.}
\end{quote}

In the transformation of values into prices of production, the
transformation is one of mere \emph{magnitude}, not \emph{substance}.
Marx is clear about this:

\begin{quote}
Price, after all, is the value of the commodity as distinct from its
use-value (and this is also the case with market price, \emph{whose
distinction from value is not qualitative, but merely quantitative,
bearing exclusively on the magnitude of value}). A price that is
\emph{qualitatively distinct} from value is an absurd contradiction.
(\emph{Capital, Vol. 3} 476, my emphasis)
\end{quote}

This is why Marx calls prices of production \emph{transformed forms of
value} and profits \emph{transformed forms of surplus value} (e.g.,
\emph{Capital, Vol. 3}, pp. 263, 267, 274). They are "made" of the same
"stuff". The Transformation Problem is the problem of transforming
prices which are equal to values into prices which bring the average
rate of profit and are therefore not equal to values. This is a merely
quantitative, not qualitative, transformation.

\subsection*{Marx's Solution}

Marx presents his solution to the Transformation Problem in chapter 9 of
\emph{Capital, Vol. 3}. Remember, the problem is how a general rate of
profit can be consistent with the law of value since the existence of a
general rate of profit entails that the profits a capital receives are
unrelated to the surplus value that that capital produces; similarly,
the price of that capital's commodity is unrelated to the amount of
labor required for its production. Marx's solution is that the total
amount of value and surplus value in the economy is determined by
labor-time and that individual profits are mere portions of this total
surplus value. Here we see the source of Marx's two famous "aggregate
equalities": total prices of production = total value and total profits
= total surplus value.

Now we can better understand why Marx says prices of production are
"transformed forms" of values (and profits are "transformed forms" of
surplus value): they are qualitatively identical ("made of the same
stuff"), and differ only quantitatively, only in magnitude. Let's
illustrate this with a simple example. Let's say that some capital's
cost-price is c=\$80 and v=\$20 and that the rate of surplus value is
\%100. The value of this capital's commodity is then
c+v+s=\$80+\$20+\$20= \$120 (where c=constant capital, v=variable
capital, and s=surplus value). If the average profit rate is \%10, then
the price of production of this capital's commodity is
c+v+p=\$80+\$20+\$10=\$110.

The difference between this commodity's value and price of production is
merely quantitative. When the commodity sells at its value, the
capitalist receives back as profit the same amount of surplus value that
his own workers produced. When the commodity sells at its price of
production, which is here less than its value, the capitalist receives
back as profit a smaller amount of surplus value than his own workers
produced. What happens to the surplus value this capitalist's workers
produced but the capitalist didn't receive as profit, the \$10
difference between value and price of production? It is received by a
different capitalist who sells his commodity at a price of production
\emph{greater} than its value, and so whose profit is \emph{greater}
than the amount of surplus value actually produced by his own workers.

Since the amount of aggregate profit is equal to the amount of aggregate
surplus value, these individual deviations of price of production from
value cancel each other out. The total surplus value, which is
completely determined by surplus labor, is simply \emph{redistributed}
so that each capitalist receives an amount of surplus value determined
by the average rate of profit, rather than the actual amount of surplus
value his own workers produced. This cancelling out of the deviations
can be seen in Marx's tables in chapter 9. An easier to read table was
made by Rubin, which I've slightly altered
\href{http://i.imgur.com/wRrqQcl.png}{here}. You can see how the same
amount of surplus value that goes unreceived by capitals of
less-than-average organic composition (c/v) is received by capitals of
greater-than-average organic composition.

Value is still determined by labor-time and surplus value is still
determined by surplus labor, so Marx has shown that a general rate of
profit is consistent with the law of value, an achievement made by no
political economist before him.

\subsection*{An Inconsistent Solution?}

The prices of commodities have been quantitatively transformed from
those that give the capitalist a profit equal to the surplus value his
workers produced to those that give him a profit determined by the
average rate. But under capitalism the inputs which make up c and v, the
means of production and labor-power, are themselves commodities
(although labor-power is not directly produced by any capital). The
capitalist buys them at \emph{their} prices of production, not their
values. The accusation of inconsistency arises here. It is alleged that
when Marx transforms the value of a commodity into its price of
production, he forgets to similarly transform the \emph{inputs} into
that same commodity. In most discussions of the Transformation Problem,
you will encounter a sentence like "Marx forgot to transform the
inputs". That's what they mean.

It is further alleged either that a consistent solution is impossible or
if there is one that \emph{one} of Marx's two aggregate equalities must
be wrong. Which one is wrong and which is right is completely arbitrary,
simply imposed by the author as an assumption.

\subsection*{Single-System Interpretations}

The position I favor claims that Marx's solution was right and needs no
correcting. According to this position, called the "single-system
interpretation," Marx does not need to transform the price of the
inputs. Why?

For Marx, this is the circuit of capital: M -\textgreater{} C {[}mp +
lp{]} ... P ... C' -\textgreater{} M'

The capitalist begins with a sum of money, M. He uses this money to buy
commodities C consisting of means of production (mp) and labor power
(lp). The arrows signify \emph{exchange}. These commodities enter the
production process, P. At the end of this production process a new
commodity is produced, C', whose value is greater than C. This commodity
is sold for a sum of money, M', which is greater than the sum originally
advanced, M. Notice that the circuit begins not with means of production
and labor power but with a sum of money, M. This sum of money is the
real and necessary input to the production process, without which the
process cannot begin. Under Marx's early assumption that commodities
exchange at their values, the value of this sum of money and the value
of C {[}mp + lp{]} are the same. When this assumption is dropped, the
value of this sum of money and the value of C {[}mp + lp{]} are not the
same. This is the \emph{key} point: \emph{A sum of money has no price of
production or any other price differing from its value}. (This is not to
say that it is impossible for someone to, say, sell \$100 for \$105, but
this properly belongs under the investigation of \emph{interest}. See
chapters 21-4 of \emph{Capital, Vol. 3}.)

The value of this initial sum of money, M, consisting of the constant
and variable capital needed to buy means of production and labor power,
need not and \emph{cannot} be transformed into a price of production.

\begin{quote}
\href{https://www.marxists.org/archive/marx/works/1863/theories-surplus-value/ch10.htm\#sa4b}{The
price of the commodity which serves as a measure of value and hence as
money, does not exist at all, because otherwise, apart from the
commodity which serves as money I would need a second commodity to serve
as money - a double measure of values.}
\end{quote}

The circuit of capital for the money commodity differs from that given
above. It is instead: {[}M -\textgreater{} C {[}mp + lp{]} ... P ... M'
\href{http://www.marxists.org/archive/marx/works/1885-c2/ch01.htm\#3}{(Capital,
Vol. 2, p. 131)}

Notice the absence of C'. Say the money commodity is gold. The circuit
of capital for gold production starts with gold and ends with a larger
sum of gold. This newly produced gold is already money, so it doesn't
have to be exchanged. Hence no need for C'-\textgreater M' in this
circuit. The surplus value created by gold producers does not have to be
realized in circulation like all other surplus value. The profit
received in the gold industry is always equal to the surplus value
produced in the gold industry. Hence the gold industry does not
participate in the redistribution of surplus value among industries.

The inputs, the initial constant and variable capital needed to buy
means of production and labor power, therefore require no
transformation. The value of constant and variable capital is the value
of this sum of money, \emph{not} the value of the means of production
and labor power that it buys. As Marx wrote:

\begin{quote}
\href{https://www.marxists.org/archive/marx/works/1863/theories-surplus-value/ch24.htm}{The
£50 of constant capital means nothing more than that it contains the
same amount of labour-time as that embodied in £50 of gold.}
\end{quote}

\subsection*{Conclusion}

This post is already too long, so I won't get into other interpretations
or objections to single-system interpretations here. We can discuss all
of this in the comments if you want. I hope I have given you some
understanding of what the Transformation Problem is and why it is
important.
