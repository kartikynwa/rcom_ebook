\section{Socialism in Cuba}

\emph{\href{https://www.reddit.com/r/communism/comments/cgjlx1/another_masterpost_on_cuban_socialism_with_sources/}{Link
to post} by /u/flesh\_eating\_turtle}

\subsection*{Introduction}

The Republic of Cuba has had an extraordinary political influence for a
nation its size. Its revolution served as an inspiration to victims of
imperialism everywhere, and its socialist model has provided an example
for the oppressed people of the world to follow. As such, it is the duty
of all socialists to be well-informed about this nation.

As always, all sources will be listed at the end.

\subsection*{Pre-Revolutionary Cuba}

Before learning about the achievements of Cuban socialism, we should
take a moment to examine what life was like before the revolution.
Fulgencio Batista's defenders will typically claim that living standards
were better before the revolution. However, a quick examination of the
facts will show this to be nonsense. It is true that Batista's reign saw
relatively high GDP growth; however, human development indicators paint
a far bleaker picture. According to a
\href{https://www.coralgablescavaliers.org/ourpages/users/099346/IB\%20History/Americas/Cuba/Cuba_s\%20Food\%20Rationing.pdf}{paper}
from Cornell University:

\begin{quote}
Opinions aside, although Cuba ranked as one of the most prosperous
developing countries in the 1950s based on gross domestic product (GDP),
social indicators for this period portray dismal social conditions,
particularly among the rural peasants.
\end{quote}

Batista's regime left the Cuban people (especially the large rural
population) mired in poverty and illness. According to the
aforementioned paper, contemporary studies reported a 91\% malnutrition
rate among agricultural workers. Though some commentators consider this
figure to be too high, "it nonetheless conveys the magnitude of rural
impoverishment." Health conditions are summarized by a
\href{https://caribbean.scielo.org/scielo.php?pid=S0043-31442013000300015\&script=sci_arttext\&tlng=pt}{study}
in the \emph{West Indian Medical Journal:}

\begin{quote}
Poor hygiene, inefficient sanitation and malnutrition {[}contributed{]}
to the infant mortality rate of 60 per 1000 lives, maternal mortality
rate of 125.3 per 1000, {[}and{]} a general mortality rate of 6.4 per
1000.
\end{quote}

The rural population in particular suffered from dismal health
conditions; according to a
\href{https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3464859/}{study}
published in the \emph{American Journal of Public Health:}

\begin{quote}
Cuba had only 1 rural hospital, only 11\% of farm worker families drank
milk, and rural infant mortality stood at 100 per 1000 live births.
\end{quote}

Infrastructure was also pitifully underdeveloped under Batista.
According to the aforementioned Cornell paper:

\begin{quote}
According to the 1953 census, 54.1 percent of rural homes had no toilets
of any kind. Only 2.3 percent of rural homes had indoor plumbing,
compared with 54.6 of urban homes. In rural areas, 9.1 percent of houses
had electricity, compared with 87 percent of houses in urban areas.
\end{quote}

Illiteracy and unemployment were widespread under Batista:

\begin{quote}
Nearly one-quarter of people 10 years of age and older could not read or
write, and the unemployment rate was 25 percent.
\end{quote}

The high illiteracy rate is hardly surprising when one remembers the
shoddy state of education in pre-revolutionary Cuba. According to an
\href{https://www.theguardian.com/global-development/poverty-matters/2011/aug/05/cuban-development-model}{article}
in the \emph{Guardian:}

\begin{quote}
In 1958, under the Batista dictatorship, half of Cuba's children did not
attend school.
\end{quote}

All of this is not even mentioning the imperialist domination, organized
crime, and rampant exploitation that the Cuban people endured throughout
Batista's reign. With all of this in mind, let us move on to examining
the Cuban revolution and its achievements.

\subsection*{Economic and Nutritional Indicators After the Revolution}

Since the very beginning, the Cuban revolution has been committed to the
improvement of life for the people in both the economic and social
spheres. According to a
\href{https://web.archive.org/web/20090303221405/http://www.oxfamamerica.org/newsandpublications/publications/research_reports/art3670.html/OA-Cuba_Social_Policy_at_Crossroads-en.pdf}{report}
from Oxfam America:

\begin{quote}
When Cuba's revolution came to power in 1959, its model of development
aimed to link economic growth with advances in social justice.
\end{quote}

According to United Nations data, the unemployment rate in Cuba
\href{http://data.un.org/en/iso/cu.html}{remains below 3\%}, as it has
for decades. Unofficial rates may be slightly higher, but even
\emph{twice} this rate would still place Cuba far below the regional
average (and \emph{far} lower than under Batista).

According to the \href{https://www.globalhungerindex.org/cuba.html}{2019
Global Hunger Index}, Cuba is one of only seventeen nations on Earth
(and only four in Latin America) to have a score lower than 5,
signifying impressively low levels of hunger. Cuba's rate of
undernourishment is below 2.5\%.

According to a
\href{https://ourworldindata.org/grapher/malnutrition-death-rates?tab=chart\&country=CUB+USA}{report}
from Our World in Data (based at the University of Oxford), Americans
are more than twice as likely as Cubans to die from malnutrition.

According to a
\href{http://www.fao.org/ag/agn/nutrition/cub_en.stm}{report} from the
FAO, "remarkably low percentages of child malnutrition put Cuba at the
forefront of developing countries."

According to a
\href{https://web.archive.org/web/20131105150934/http://www.fas.usda.gov/itp/cuba/CubaSituation0308.pdf}{report}
from the United States Department of Agriculture, the average Cuban
consumes approx. 3300 calories per day, far above the Latin American and
Caribbean average, and only slightly lower than in the United States.
Approx. 2/3 of nutritional needs are met by monthly food rations, while
the rest is bought independently. The report also states:

\begin{quote}
The Cuban economy has made remarkable progress toward recovery from the
economic disaster generated by the collapse of the Soviet Bloc.
\end{quote}

In its \href{https://www1.wfp.org/countries/cuba}{report on Cuba}, the
World Food Program (the food-assistance branch of the United Nations)
states that that:

\begin{quote}
Over the last 50 years, comprehensive social protection programs have
largely eradicated poverty and hunger. Food-based social safety nets
include a monthly food basket for the entire population, school feeding
programs, and mother-and-child health care programs.
\end{quote}

This is especially impressive when Cuba is compared to other developing
countries, and considering the decades of economic blockade that the
nation has endured. The report also states:

\begin{quote}
The largest island in the Caribbean, Cuba ranks 72th out of 189
countries in the 2019 Human Development Index and is one of the most
successful in achieving the Millennium Development Goals (MDGs).
\end{quote}

An
\href{https://www.theguardian.com/global-development/poverty-matters/2010/sep/30/millennium-development-goals-cuba}{article}
in the \emph{Guardian} addresses this topic:

\begin{quote}
...the evidence suggests that Cuba has made excellent progress towards
the MDGs in the last decade, building on what are already universally
acknowledged to be outstanding achievements in equitable health and
education standards.

According to a new MDG Report Card by the Overseas Development
Institute, Cuba is among the 20 best performing countries in the world.
\end{quote}

The article also includes a statement from a Cuban economist on how this
progress is made:

\begin{quote}
The Cuban economy is planned and we redistribute income from the most
dynamic sectors, which generate most foreign exchange, towards those
that are less dynamic but necessary for the country. That's how we
maintain a budget to keep health and education high quality and free of
charge to the user.
\end{quote}

The revolution greatly improved the housing situation in Cuba, and also
brought significant urban development. According to Oxfam America:

\begin{quote}
Initiatives in the cities were no less ambitious. Urban reform brought a
halving of rents for Cuban tenants, opportunities for tenants to own
their housing, and an ambitious program of housing construction for
those living in marginal shantytowns. New housing, along with the
implementation of measures to create jobs and reduce unemployment,
especially among women, rapidly transformed the former shantytowns.
\end{quote}

Finally, the social security and pensions system in Cuba has drastically
improved since the revolution, as evidenced by this statement from the
aforementioned Oxfam America report:

\begin{quote}
Both coverage and distribution have improved significantly since the
revolution. With a pension system since the 1930's, Cuba was one of the
first Latin American countries to establish one. It consisted of
independent pension funds and by 1959 covered about 63\% of workers, but
the system varied greatly in terms of benefits and relied almost
exclusively on workers' contributions. Since 1959, the program has been
funded completely by the government. In 1958, about 63\% of the labor
force was covered for old age, disability, and survivors insurance;
today, the coverage is universal.
\end{quote}

\subsection*{Sustainable Development and Environmental Preservation}

According to a
\href{https://www.sciencedirect.com/science/article/pii/S0921800919303386}{study}
in the journal \emph{Ecological Economics,} Cuba is
\href{https://www.forbes.com/sites/christinero/2019/12/01/every-country-is-developing-according-to-the-new-sustainable-development-index/\#4ce6dd7618bc}{the
most sustainably developed country in the world}. This is based on the
\href{https://www.sustainabledevelopmentindex.org/}{Sustainable
Development Index}, which measures a nation's human development outcomes
(health and education, per-capita income, etc.) and factors in the
country's environmental impact. This result was confirmed in a separate
\href{https://www.telesurenglish.net/news/As-World-Burns-Cuba-Number-1-for-Sustainable-Development-WWF-20161027-0018.html}{report
from the World Wildlife Fund}.

Cuba is also
\href{https://www.footprintnetwork.org/2015/09/23/eight-countries-meet-two-key-conditions-sustainable-development-united-nations-adopts-sustainable-development-goals/}{one
of the only nations to meet conditions for sustainable development}, and
has been
\href{https://wwf.panda.org/wwf_news/?1944/the-other-cuban-revolution}{praised
by the WWF} for its "enlightened environmental policies." Considering
the increasingly urgent threat posed by climate change and environmental
catastrophe, Cuba provides a model for the rest of the world to aspire
to.

\subsection*{Healthcare Indicators}

Cuba's healthcare system is one of its most impressive and well-known
achievements. According to the aforementioned
\href{https://www.coralgablescavaliers.org/ourpages/users/099346/IB\%20History/Americas/Cuba/Cuba_s\%20Food\%20Rationing.pdf}{paper}
from Cornell University:

\begin{quote}
Cuba's superior health indicators---highly ranked both regionally and
globally---are attributed to the country's universal primary healthcare
services.
\end{quote}

The Cuban health system is based on public investment and universal
provision. According to a
\href{http://www.ecocubanetwork.net/wp-content/uploads/NASW\%20CUBA\%20REPORT.pdf}{report}
from the National Association of Social Workers:

\begin{quote}
Cuba has the largest number of doctors per capita of any country in the
world... the country devotes almost a quarter of its gross domestic
product (GDP) to education and health care---nearly twice the percentage
of U.S. GDP allotted to the same expenses. As a result, the country
guarantees free education and health care for all citizens, and women
receive six weeks of paid prenatal maternity leave and up to one year of
paid leave after giving birth.
\end{quote}

According to
\href{https://data.worldbank.org/indicator/SP.DYN.LE00.IN?locations=CU-US}{data}
from the World Bank, Cuba's life expectancy is slightly \emph{longer}
than that of the United States. Compare this to the pre-revolutionary
era, when the Cuban life expectancy was approximately six years shorter
than the American life expectancy.

Also, according to
\href{https://data.worldbank.org/indicator/SP.DYN.IMRT.IN?locations=CU-US}{data}
from the World Bank, Cuba's infant mortality rate is approximately
one-third \emph{lower} than that of the United States. Compare this to
the pre-revolutionary era, when the Cuban infant mortality rate was
nearly \emph{double} that of the USA.

According to a 2019
\href{https://journals.sagepub.com/doi/abs/10.1177/0169796X19826731}{study}
published in the \emph{Journal of Developing Societies}, Cuba's health
indicators surpass those of developed nations, despite far lower
expenditures:

\begin{quote}
While Cuba spends about one-twentieth per capita on healthcare compared
to the USA... people in Cuba nevertheless enjoy longer life expectancy
(79 years) than do people in the USA (78 years)... Cuba also has a
superior childhood mortality rate (the number of deaths to age 5 per
1,000 live births per year) of six, compared to eight in the USA.
\end{quote}

Cuba has also made some amazing healthcare developments. According to
the WHO,
\href{https://www.who.int/mediacentre/news/releases/2015/mtct-hiv-cuba/en/}{Cuba
is the first nation in the world to eliminate mother-to-child HIV and
syphilis transmission}.

According to the \emph{Washington Post},
\href{https://www.washingtonpost.com/news/to-your-health/wp/2016/10/27/in-a-first-u-s-trial-to-test-cuban-lung-cancer-vaccine/?utm_term=.9a6205587548}{Cuba
has developed a potential vaccine against lung cancer}, which has shown
very promising results, and is being tested in the USA.

The Cuban experience provides an important model for other nations to
follow. According to a
\href{https://academic.oup.com/ije/article/35/4/817/686547}{study} in
the \emph{International Journal of Epidemiology} (published by the
Oxford University Press):

\begin{quote}
Cuba represents an important alternative example where modest
infrastructure investments combined with a well-developed public health
strategy have generated health status measures comparable with those of
industrialized countries... If the Cuban experience were generalized to
other poor and middle-income countries human health would be
transformed.
\end{quote}

An
\href{https://www.theguardian.com/news/gallery/2007/jul/17/internationalnews}{article}
in the \emph{Guardian} summarizes this topic quite well:

\begin{quote}
Whether it is a consultation, dentures or open heart surgery, citizens
are entitled to free treatment. As a result the impoverished island
boasts better health indicators than its exponentially richer neighbour
90 miles across the Florida straits.
\end{quote}

This is an enormously impressive achievement.

\subsection*{Educational Developments}

Since the revolution, enormous strides have been made in education. One
of the most significant developments was the National Literacy Campaign,
spearheaded by Che Guevara. According to
\href{https://web.archive.org/web/20090303221405/http://www.oxfamamerica.org/newsandpublications/publications/research_reports/art3670.html/OA-Cuba_Social_Policy_at_Crossroads-en.pdf}{Oxfam
America}:

\begin{quote}
The National Literacy Campaign of 1961, recognized as one of the most
successful initiatives of its kind, mobilized teachers, workers, and
secondary school students to teach more than 700,000 persons how to
read. This campaign reduced the illiteracy rate from 23\% to 4\% in the
space of one year.
\end{quote}

Before the revolution, literacy in Cuba was between 60\% and 76\%,
depending on the estimates used. Today, the
\href{https://www.cia.gov/library/publications/the-world-factbook/geos/cu.html}{CIA
World Factbook} gives Cuba's literacy rate as 99.8\%.

In addition,
\href{https://www.cia.gov/library/publications/the-world-factbook/fields/369rank.html\#CU}{Cuba
spends a higher percentage of GDP on education than any other nation in
the world}. This has resulted in impressive results; according to a 2014
\href{https://www.worldbank.org/content/dam/Worldbank/document/LAC/Great_Teachers-How_to_Raise_Student_Learning-Barbara-Bruns-Advance\%20Edition.pdf}{study}
from the World Bank,
\href{https://www.huffpost.com/entry/world-bank-cuba-has-the-b_b_5925864}{Cuba
has the only "high quality" educational system in Latin America}.

\subsection*{Infrastructural Developments}

In 1959, approx. 50\% of Cuban households had access to electricity.
According to
\href{https://www.edf.org/sites/default/files/cuban-electric-grid.pdf}{a
report from the Environmental Defense Fund}, by 1989, more than 95\% of
households had access to electricity, including in rural areas, which
had previously been almost entirely deprived. Cuba also surpassed many
of its neighbors in terms of electricity generation:

\begin{quote}
By 1990 Cuba had roughly 1.8 times more generating capacity per person
than the Dominican Republic and 1.3 times more than Jamaica.
\end{quote}

In Cuba, \href{http://data.un.org/en/iso/cu.html}{access to clean water
and sanitation has greatly improved since the revolution}. According to
United Nations data, as of 2018, 96.4\% of the urban population and
89.8\% of the rural population had access to clean drinking water, while
94.4\% of the urban population and 89.1\% of the rural population had
access to improved sanitation services:

An excellent
\href{https://www.independent.co.uk/voices/fidel-castro-death-cuba-embargo-peasants-poor-urban-areas-modernisation-a7441036.html}{article}
in the \emph{Independent} discussed this issue quite well:

\begin{quote}
This is Fidel's legacy. Clean water and electricity for all. And
universal free education and healthcare. Cubans often joke that they're
healthier and better educated than Americans despite the 50-year-plus US
blockade.

So for me, rural Cuba is Fidel's Cuba. His ideals live on here -- and
the rural poor of Cuba have benefited the most from his cradle-to-grave
policies. Here, the grandchildren of peasants really do go on to become
consultant surgeons and commercial airline pilots.
\end{quote}

This is an enormous credit to the revolution.

\subsection*{Social Policy}

The Cuban revolution has also made great strides in eliminating
discrimination and inequality. As the
\href{https://web.archive.org/web/20090303221405/http://www.oxfamamerica.org/newsandpublications/publications/research_reports/art3670.html/OA-Cuba_Social_Policy_at_Crossroads-en.pdf}{report}
from Oxfam America states:

\begin{quote}
Social policy has also favored the development of equity across society,
including the equitable distribution of benefits across all sectors of
the population, sometimes favoring the most vulnerable. In the last 40
years Cubans have greatly reduced differences in income between the
lowest and the highest paid persons. Women have benefited significantly
from the revolution as they have educated themselves and entered the
labor force in large numbers. The differences among Cubans of different
races have also been reduced.
\end{quote}

Considering the widespread racial and gender discrimination that existed
before the revolution, these achievements must be admired.

\subsection*{Popular Opinion in Cuba}

According to an
\href{https://newrepublic.com/article/121502/cuban-poll-shows-more-political-satisfaction-americans}{article}
published in the \emph{New Republic}, Cubans are significantly more
satisfied with their political system than Americans are with theirs.
The same holds true for the healthcare and education systems:

\begin{quote}
More than two-thirds of Cubans---68 percent---are satisfied with their
health care system. About 66 percent of Americans said the same in a
November 2014 Gallup poll. Seventy-two percent of Cubans are satisfied
with their education system, while an August 2014 Gallup poll found that
less than half of Americans---48 percent---are ``completely'' or
``somewhat'' satisfied with the quality of K-12 education.
\end{quote}

The Cuban people also recently ratified a new constitution, which
reasserts the role of the Communist Party, and affirms that Cuba is a
socialist state advancing towards communism. The constitution also
includes some political and economic reforms, such as the recognition of
small businesses, and the presumption of innocence in the court system.
According to an
\href{https://www.reuters.com/article/us-cuba-constitution-referendum-idUSKCN1QE22Y}{article}
from Reuters, independent evidence supports the official vote tally
(approx. 90\% support):

\begin{quote}
The independent online newspaper \emph{El Toque} asked readers to send
in local tallies, a dozen of which showed overwhelming support for
ratification.
\end{quote}

Yoani Sanchez, "Cuba's best-known dissident," witnessed the count at her
local polling station, reporting the results as "400 yes votes, 25 no
votes and 4 blank ballots." This suggests that the official results were
correct, and the Cuban people did overwhelmingly support the new
constitution.

An
\href{https://www.independent.co.uk/voices/fidel-castro-death-cuba-embargo-peasants-poor-urban-areas-modernisation-a7441036.html}{article}
in the \emph{Independent}, written by an author whose family lives in
Cuba, sums this issue up well:

\begin{quote}
Most Cubans I speak to support the reshaping of the economy and the
greater ties with the US. Just like us, they want to better their lives,
they want a better mobile phone, a bigger house, they want to travel.
But none of them would want to live in a Cuba, no matter how rich,
without universal free education, free healthcare, cheap public
transport and the lowest rates of violent crime in the Americas. None of
them. This is Fidel's legacy.
\end{quote}

While the Cuban people largely support economic reform and normalization
of relations with the USA, their overall support for the achievements of
their socialist system remains high. As the \emph{New Republic} puts it:

\begin{quote}
Objective indicators, like the country's low infant mortality and
illiteracy rates, have long shown that Cuba has relatively strong social
services. This new polling data suggests that Cubans are well aware of
it.
\end{quote}

This is an important credit to the revolution.

\subsection*{"But What About the Cuban Exiles?"}

The most common argument against Cuban socialism is that the Cuban exile
population (and their strong distaste for socialism) somehow "proves"
that socialist Cuba is terrible. However, this omits a key fact: the
exiles come primarily from the wealthy class of Cuba. According to a
\href{https://www.jstor.org/stable/799194?seq=1\#metadata_info_tab_contents}{study}
in the journal \emph{Social Problems} (published by the Oxford
University Press):

\begin{quote}
Comparison of the occupational, age, and educational composition of the
community with the Cuban population indicates that the refugees are
better educated and come from higher status occupations than the
population from which they have exiled themselves... more recent exiles
are more representative of the Cuban population, but the rural worker is
still vastly underrepresented.
\end{quote}

Another thing to consider is that the exile took place during a time of
conflict and difficulty for Cuba; the revolution was still very new, and
the government had not entirely established itself yet. This likely
explains why there were some outliers (i.e. exiles from the
working-class population), although the majority were still from the
wealthy sectors of Cuban society.

\subsection*{Conclusion}

Cuba is a nation with many problems; the economy has slowed since the
fall of the Soviet Union (losing your only major trading partner tends
to hurt a nation's economy), and international pressure from the USA
continues to place Cuba under strain. However, the enormous achievements
of the revolution cannot be overlooked; Cuba has provided first-world
health and educational standards on a third-world budget, as well as
above-average nutrition and infrastructure, all while standing up to the
world's most powerful imperialist force, only ninety miles off its
shores.

One struggles to find a proper statement with which to sum up the
achievements of the Cuban revolution. Perhaps this one, from former UN
Secretary General Kofi Annan (from April 11, 2000):

\begin{quote}
Cuba's achievements in social development are impressive given the size
of its gross domestic product per capita. As the human development index
of the United Nations makes clear year after year, Cuba should be the
envy of many other nations, ostensibly far richer. {[}Cuba{]}
demonstrates how much nations can do with the resources they have if
they focus on the right priorities -- health, education, and literacy.
\end{quote}

Perhaps the best statement is given by Aviva Chomsky in her book
\href{https://edisciplinas.usp.br/pluginfile.php/4447044/mod_resource/content/1/A-History-of-the-Cuban-Revolution.pdf}{A
History of the Cuban Revolution}:

\begin{quote}
The Revolution has been wildly audacious, experimental, and diverse. It
has evolved under often adverse circumstances. It created unprecedented
socioeconomic equality, and showed the world that it is indeed possible
for a poor, Third World country to feed, educate, and provide health
care for its population... If we want to imagine a better world for all
of us, I can think of no better place to start than by studying the
Cuban Revolution.
\end{quote}

That's something which we can all appreciate.

\subsection*{Sources}

\begin{itemize}
\tightlist
\item
  \href{https://www.coralgablescavaliers.org/ourpages/users/099346/IB\%20History/Americas/Cuba/Cuba_s\%20Food\%20Rationing.pdf}{Cornell
  University \textbar{} Cuba's Food-Rationing System and Alternatives}
\item
  \href{https://caribbean.scielo.org/scielo.php?pid=S0043-31442013000300015\&script=sci_arttext\&tlng=pt}{West
  Indian Medical Journal \textbar{} Cuba: Healthcare and the Revolution}
\item
  \href{https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3464859/}{American
  Journal of Public Health \textbar{} The Curious Case of Cuba}
\item
  \href{https://www.theguardian.com/global-development/poverty-matters/2011/aug/05/cuban-development-model}{The
  Guardian \textbar{} Cuba: A Development Model That Proved the Doubters
  Wrong}
\item
  \href{https://web.archive.org/web/20090303221405/http://www.oxfamamerica.org/newsandpublications/publications/research_reports/art3670.html/OA-Cuba_Social_Policy_at_Crossroads-en.pdf}{Oxfam
  America \textbar{} Cuba: Social Policy at the Crossroads}
\item
  \href{http://data.un.org/en/iso/cu.html}{UNdata \textbar{} Country
  Profiles: Cuba}
\item
  \href{https://www.globalhungerindex.org/cuba.html}{2019 Global Hunger
  Index \textbar{} Cuba}
\item
  \href{https://ourworldindata.org/grapher/malnutrition-death-rates?tab=chart\&country=CUB+USA}{Our
  World in Data \textbar{} Deaths by Malnutrition in the USA and Cuba}
\item
  \href{http://www.fao.org/ag/agn/nutrition/cub_en.stm}{Food and
  Agricultural Organization (United Nations) \textbar{} Report on
  Nutrition in Cuba}
\item
  \href{https://web.archive.org/web/20131105150934/http://www.fas.usda.gov/itp/cuba/CubaSituation0308.pdf}{USDA
  \textbar{} Cuba Food and Agricultural Situation}
\item
  \href{https://www1.wfp.org/countries/cuba}{World Food Program USA
  (United Nations) \textbar{} Cuba Has "Largely Eliminated Hunger and
  Poverty"}
\item
  \href{https://www.theguardian.com/global-development/poverty-matters/2010/sep/30/millennium-development-goals-cuba}{The
  Guardian \textbar{} Why We Should Applaud Cuba's Progress Towards the
  Millennium Development Goals}
\item
  \href{https://www.sciencedirect.com/science/article/pii/S0921800919303386}{Ecological
  Economics \textbar{} The Sustainable Development Index: Measuring the
  Ecological Efficiency of Human Development in the Anthropocene}
\item
  \href{https://www.forbes.com/sites/christinero/2019/12/01/every-country-is-developing-according-to-the-new-sustainable-development-index/\#4ce6dd7618bc}{Forbes
  \textbar{} Every Country in the World is Developing, According to the
  New Sustainable Development Index}
\item
  \href{https://www.telesurenglish.net/news/As-World-Burns-Cuba-Number-1-for-Sustainable-Development-WWF-20161027-0018.html}{Telesur
  \textbar{} As World Burns, Cuba Number One for Sustainable
  Development}
\item
  \href{https://www.footprintnetwork.org/2015/09/23/eight-countries-meet-two-key-conditions-sustainable-development-united-nations-adopts-sustainable-development-goals/}{Global
  Footprint Network \textbar{} Only Eight Countries Meet Two Key
  Conditions for Sustainable Development, as United Nations Adopts
  Sustainable Development Goals}
\item
  \href{https://wwf.panda.org/wwf_news/?1944/the-other-cuban-revolution}{World
  Wildlife Fund \textbar{} The Other Cuban Revolution}
\item
  \href{http://www.ecocubanetwork.net/wp-content/uploads/NASW\%20CUBA\%20REPORT.pdf}{National
  Association of Social Workers \textbar{} Social Services in Cuba}
\item
  \href{https://data.worldbank.org/indicator/SP.DYN.LE00.IN?locations=CU-US}{World
  Bank \textbar{} Life Expectancy in the USA and Cuba}
\item
  \href{https://data.worldbank.org/indicator/SP.DYN.IMRT.IN?locations=CU-US}{World
  Bank \textbar{} Infant Mortality in the USA and Cuba}
\item
  \href{https://journals.sagepub.com/doi/abs/10.1177/0169796X19826731}{Journal
  of Developing Societies \textbar{} Cuban Public Healthcare: A Model of
  Success for Developing Nations}
\item
  \href{https://www.who.int/mediacentre/news/releases/2015/mtct-hiv-cuba/en/}{World
  Health Organization (United Nations) \textbar{} Cuba First Country in
  the World to Eliminate Mother-to-Child HIV Transmissions}
\item
  \href{https://www.washingtonpost.com/news/to-your-health/wp/2016/10/27/in-a-first-u-s-trial-to-test-cuban-lung-cancer-vaccine/?utm_term=.9a6205587548}{The
  Washington Post \textbar{} USA to Test Cuban Lung-Cancer Vaccine}
\item
  \href{https://academic.oup.com/ije/article/35/4/817/686547}{International
  Journal of Epidemiology \textbar{} Health in Cuba}
\item
  \href{https://www.theguardian.com/news/gallery/2007/jul/17/internationalnews}{The
  Guardian \textbar{} Healthcare in Cuba}
\item
  \href{https://www.cia.gov/library/publications/the-world-factbook/geos/cu.html}{CIA
  World Factbook \textbar{} Cuba}
\item
  \href{https://www.cia.gov/library/publications/the-world-factbook/fields/369rank.html\#CU}{CIA
  World Factbook \textbar{} Country Comparison: Educational Expenditures
  by Nation}
\item
  \href{https://www.worldbank.org/content/dam/Worldbank/document/LAC/Great_Teachers-How_to_Raise_Student_Learning-Barbara-Bruns-Advance\%20Edition.pdf}{World
  Bank \textbar{} Great Teachers: How to Raise Student Learning in Latin
  America and the Caribbean}
\item
  \href{https://www.huffpost.com/entry/world-bank-cuba-has-the-b_b_5925864}{HuffPost
  \textbar{} World Bank: Cuba Has the Best Education System in Latin
  America and the Caribbean}
\item
  \href{https://www.edf.org/sites/default/files/cuban-electric-grid.pdf}{Environmental
  Defense Fund \textbar{} The Cuban Electric Grid}
\item
  \href{http://data.un.org/en/iso/cu.html}{UNdata \textbar{} Country
  Profiles: Cuba}
\item
  \href{https://newrepublic.com/article/121502/cuban-poll-shows-more-political-satisfaction-americans}{New
  Republic \textbar{} Polls Show Cubans Have Higher Political
  Satisfaction Than Americans}
\item
  \href{https://www.reuters.com/article/us-cuba-constitution-referendum-idUSKCN1QE22Y}{Reuters
  \textbar{} Cubans Overwhelmingly Ratify New Socialist Constitution}
\item
  \href{https://www.independent.co.uk/voices/fidel-castro-death-cuba-embargo-peasants-poor-urban-areas-modernisation-a7441036.html}{The
  Independent \textbar{} My Family Live in Cuba - The People May Be
  Poor, But Fidel Castro's Legacy Will Live On}
\item
  \href{https://www.jstor.org/stable/799194?seq=1\#metadata_info_tab_contents}{Oxford
  University Press \textbar{} Cubans in Exile: A Demographic Analysis}
\item
  \href{https://edisciplinas.usp.br/pluginfile.php/4447044/mod_resource/content/1/A-History-of-the-Cuban-Revolution.pdf}{"A
  History of the Cuban Revolution" by Aviva Chomsky}
\end{itemize}
