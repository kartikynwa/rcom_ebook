\section{Masterpost on the Union of Soviet Socialist Republics}

\emph{\href{https://www.reddit.com/r/communism/comments/cvgnzm/masterpost_on_the_union_of_soviet_socialist/}{Link
to post} by /u/flesh\_eating\_turtle}

\subsection*{Introduction}

The Union of Soviet Socialist Republics was the first nation ever to
declare itself a socialist state, dedicated to the building of
communism. Over the seven decades of its existence, the USSR went
through many stages and phases of development, from a semi-capitalist
feudal society, to a state capitalist nation during the NEP years, to a
developed socialist country, and finally to a revisionist state
regressing towards capitalism. The purpose of this post is to examine
the achievements of socialism in the USSR up to the early 1960's, at
which point market reforms and capitalist restoration began to take
effect. We will also be examining the disastrous effects that these
reforms had on the health and well-being of the Soviet people.

This post will consist of three parts. This first part is a discussion
of the achievements of Soviet socialism in terms of economic
development, living standards, and healthcare. The question of Stalin
and the Ukrainian famine will also be discussed. The second part will be
a discussion of Soviet advances in women's rights, as well as opposition
to racism (both internally, and in its global manifestation of
imperialism). We will also discuss the nature of Soviet democracy. The
third part will discuss the eventual fall of the USSR, and the
disastrous effects of capitalist restoration and revisionism in the
USSR.

All sources are listed at the end of the post. I will indicate which
source I am using each time I quote from one. Now, let us begin.

\subsection*{Revolution, War Communism, and the NEP}

When the 1917 revolution took place, Russia was a backwards,
semi-capitalist feudal society. The manor system had only recently been
abolished, and replaced by the most brutal and primitive form of
capitalism. The nation was dreadfully under-developed, with no sign of
improving in the future. Not only that, but what little growth did occur
led to massive inequalities. According to Professor
\href{https://nyuad.nyu.edu/en/academics/divisions/social-science/faculty/robert-allen.html}{Robert
Allen} (formerly of Oxford University, now at NYU):

\begin{quote}
Not only were the bases of Imperial advance narrow, but the process of
growth gave rise to such inequitable changes in income distribution that
revolution was hardly a surprise. Real wages for urban workers were
static in the late Imperial period despite a significant increase in
output per worker.... The revolution was also a peasant revolt, and the
interests of the peasants were different... As in the cities, there was
no gain in real wages.
\end{quote}

\href{https://homepages.warwick.ac.uk/~syrbe/}{Simon Clarke}, Professor
Emeritus of Sociology at the University of Warwick, supports these
claims:

\begin{quote}
Agriculture had reached North American levels of productivity by 1913
and wheat prices collapsed after 1914. The expansion of the railroads
had run its course and there was no prospect of protected light industry
becoming internationally competitive. The appropriate comparators for
the prospects for Russian capitalism in the twentieth century are not
Japan but Argentina or even India. Moreover, Russian capitalist
development had brought little if any benefit to the urban and rural
working class, intensifying the class conflicts that erupted in
Revolution.
\end{quote}

With the 1917 revolution (and after the bloody civil war, with its
policy of war communism), the Soviet economy began to grow rapidly. The
New Economic Policy (which nationalized large-scale industry and
redistributed land, while allowing for the private sale of agricultural
surplus) succeeded in transforming Russia from a semi-capitalist society
into a developing state capitalist society, laying the groundwork for
socialism. Professor Clarke states:

\begin{quote}
Following War Communism, the New Economic Policy (NEP) sought to develop
the Russian economy within a quasi-capitalist framework.
\end{quote}

However, economic circumstances came to require the transition to a
planned socialist economy:

\begin{quote}
However, the institutional and structural barriers to Russian economic
development were now compounded by the unfavorable circumstances of the
world economy, so that there was no prospect of export-led development,
while low domestic incomes provided only a limited market for domestic
industry. Without a state coordinated investment program, the Soviet
economy would be caught in the low-income trap typical of the
underdeveloped world.
\end{quote}

Thus, the material conditions of the time made the transition to a
socialist economy a necessity.

\subsection*{Economic Development and Living Standards in the Socialist
Era}

In 1928 (after Stalin came to power as head of the Communist Party),
Soviet Russia instituted a fully planned economy, and the first Five
Year Plan was enacted. This resulted in rapid economic growth. According
to Professor Allen:

\begin{quote}
Soviet GDP increased rapidly with the start of the first Five Year Plan
in 1928... The expansion of heavy industry and the use of output targets
and soft-budgets to direct firms were appropriate to the conditions of
the 1930's, they were adopted quickly, and they led to rapid growth of
investment and consumption.
\end{quote}

Bourgeois economists often alleged that this rapid growth came at the
cost of per-capita consumption and living standards. However, more
recent research has shown this to be false. Professor Allen states:

\begin{quote}
There has been no debate that `collective consumption' (principally
education and health services) rose sharply, but the standard view was
that private consumption declined. Recent research, however, calls that
conclusion into question... While investment certainly increased
rapidly, \emph{recent research shows that the standard of living also
increased briskly.}
\end{quote}

Calorie consumption rose rapidly during this period:

\begin{quote}
Calories are the most basic dimension of the standard of living, and
their consumption was higher in the late 1930's than in the 1920's... In
1895-1910, calorie availability was only 2100 per day, which is very low
by modern standards. By the late 1920's, calorie availability advanced
to 2500... By the late 1930's, the recovery of agriculture increased
calorie availability to 2900 per day, a significant increase over the
late 1920's. The food situation during the Second World War was severe,
but by 1970 calorie consumption rose to 3400, which was on a par with
western Europe.
\end{quote}

Overall, the development of the Soviet economy during the socialist
period was extremely impressive. According to Professor Allen:

\begin{quote}
The Soviet economy performed well... Planning led to high rates of
capital accumulation, rapid GDP growth, and rising per capita
consumption even in the 1930's.
\end{quote}

The USSR's growth during the socialist period exceeded that of the
capitalist nations:

\begin{quote}
The USSR led the non-OECD countries and, indeed, achieved a growth rate
in this period that exceeded the OECD catch-up regression as well as the
OECD average.
\end{quote}

This success is also attributed specifically to the revolution and the
socialist system. As Professor Allen states:

\begin{quote}
This success would not have occurred without the 1917 revolution or the
planned development of state owned industry.
\end{quote}

The benefits of the socialist system are obvious upon closer study. As
the Professor Clarke puts it:

\begin{quote}
...a capitalist economy would not have created the industrial jobs
required to employ the surplus labour, since capitalists would only
employ labour so long as the marginal product of labour exceeded the
wage. State-sponsored industrialization faced no such constraints, since
enterprises were encouraged to expand employment in line with the
demands of the plan.
\end{quote}

Economic growth was also aided by the liberation of women, and the
resulting control over the birth rate, as well as women's participation
in the workforce. Allen states:

\begin{quote}
The rapid growth in per capita income was contingent not just on the
rapid expansion of GDP but also on the slow growth of the population.
This was primarily due to a rapid fertility transition rather than a
rise in mortality from collectivization, political repression, or the
Second World War. Falling birth rates were primarily due to the
education and employment of women outside the home. These policies, in
turn, were the results of enlightenment ideology in its communist
variant.
\end{quote}

Reviews of Allen's work have backed up his statements. According to the
Professor Clarke:

\begin{quote}
Allen shows that the Stalinist strategy worked, in strictly economic
terms, until around 1970... Allen's book convincingly establishes the
superiority of a planned over a capitalist economy in conditions of
labour surplus (which is the condition of most of the world most of the
time).
\end{quote}

Other studies have backed-up the findings that the USSR's living
standards rose rapidly. According to Professor
\href{https://www.ebrainerd.com/}{Elizabeth Brainerd} (formerly of
Williams College, now at Brandeis University):

\begin{quote}
Remarkably large and rapid improvements in child height, adult stature
and infant mortality were recorded from approximately 1945 to 1970...
Both Western and Soviet estimates of GNP growth in the Soviet Union
indicate that GNP per capita grew in every decade in the postwar era, at
times far surpassing the growth rates of the developed western
economies... The conventional measures of GNP growth and household
consumption indicate a long, uninterrupted upward climb in the Soviet
standard of living from 1928 to 1985; even Western estimates of these
measures support this view, albeit at a slower rate of growth than the
Soviet measures.
\end{quote}

Unfortunately, after the introduction of market reforms and other
revisionist policies, living standards began to deteriorate (although
some measures continued to increase, albeit more slowly). According to
Professor Brainerd:

\begin{quote}
Three different measures of population health show a consistent and
large improvement between approximately 1945 and 1969: child height,
adult height and infant mortality all improved significantly during this
period. These three biological measures of the standard of living also
corroborate the evidence of some deterioration in living conditions
beginning around 1970, when infant and adult mortality were rising and
child and adult height stopped increasing and in some regions began to
decline.
\end{quote}

Economic growth also began to slow around this time. According to
Professor Allen:

\begin{quote}
After the Second World War, the Soviet economy resumed rapid growth. By
1970, the growth rate was sagging, and per capita output was static by
1985.
\end{quote}

The Cold War was another factor which contributed to slowing growth
rates:

\begin{quote}
The Cold War was an additional factor that lowered Soviet growth after
1968. The creation of high tech weaponry required a disproportionate
allocation of R \& D personnel and resources to the military. Innovation
in civilian machinery and products declined accordingly. Half of the
decreased in the growth rate of per capita GDP was due to the decline in
productivity growth, and that decrease provides an upper bound to the
impact of the arms race with the United States.
\end{quote}

In short, the USSR achieved massively positive economic results until
the 1970's, when revisionist policies and the Cold War began to cause a
stagnation. Now, let us move on from economic development, and talk
about the health standards of the Soviet population.

\subsection*{-** **Healthcare Conditions in the Socialist Period -}

Health conditions in Czarist Russia had been deplorable; it was among
the unhealthiest nations in Europe (arguably in the entire world).
According to Professor Reiner~Dinkel (University of Munich):

\begin{quote}
Without doubt the Soviet Union was one of the most underdeveloped
European countries at the time of the October Revolution. In terms of
life-expectancy it lagged behind the other industrialized countries of
Europe by a gap of about 15 years.
\end{quote}

However, after the socialist revolution, healthcare conditions began to
increase rapidly. By the end of the socialist period, healthcare
standards (measured by life expectancy and mortality rates) were
superior to those of Western Europe and the USA. Professor Dinkel
states:

\begin{quote}
One of the most striking advances of socialism has been and was
generally seen to be the improvement in public health provision for the
population as a whole. In accordance with this assumption
mortality-rates in the Soviet Union declined rapidly in the first two
decades after World War II. In 1965 life-expectancy for men and women in
all parts of the Soviet Union, which still included vast underdeveloped
regions with unfavorable living conditions, were as high or even higher
than in the United States. Such a development fits perfectly into the
picture of emerging industrial development and generally improving
conditions of living.
\end{quote}

Even reactionary intellectuals were forced to acknowledge these
achievements; according to Nicholas Ebserstadt (a conservative
think-tank adviser), healthcare standards in the Soviet Union during the
socialist period surpassed those of the USA and Western Europe:

\begin{quote}
Over much of this century the nation in the vanguard of the revolution
in health was the Soviet Union. In 1897 Imperial Russia offered its
people a life expectancy of perhaps thirty years. In European Russia,
from what we can make out, infant mortality (that is, death in the first
year) claimed about one child in four, and in Russia's Asian hinterlands
the toll was probably closer to one in three. Yet by the late 1950's the
average Soviet citizen could expect to live 68.7 years: longer than his
American counterpart, who had begun the century with a seventeen-year
lead. By 1960 the Soviet infant mortality rate, higher than any in
Europe as late as the Twenties, was lower than that of Italy, Austria,
or East Germany, and seemed sure to undercut such nations as Belgium and
West Germany any year.
\end{quote}

He even notes that these achievements made socialism seem nearly
unbeatable:

\begin{quote}
In the face of these and other equally impressive material
accomplishments, Soviet claims about the superiority of their
``socialist'' system, its relevance to the poor countries, and the
inevitability of its triumph over the capitalist order were not easily
refuted.
\end{quote}

While health conditions did start to decline after the introduction of
revisionist policies in the mid-60's (this will be discussed in more
detail in part three), the healthcare achievements of the socialist
system remain unimpeachable.

\subsection*{The Question of Stalin}

(EDIT: An
\href{https://www.reddit.com/r/communism/comments/e8kpow/masterpost_on_joseph_stalin_and_the_great_purge/}{entire
masterpost on this topic} has been prepared, with more extensive sources
and detail. I recommend going there instead for a more in-depth and
detailed look at this issue.)

Joseph Stalin was the principal architect of the socialist period in the
USSR. As a result, he has been the victim of perhaps the most extensive
smear campaign in modern history. Claims that he killed tens of millions
of people, jailed victims without cause, and deliberately starved
Ukrainian peasants are only some of the propaganda charges leveled
against him. As such, it is the duty of any informed socialist to combat
this propaganda.

Firstly, we must remember the extensive achievements discussed above,
which vastly improved life for hundreds of millions of people. These
achievements were the result of the socialist system, built primarily
under Joseph Stalin. Even reactionaries have been unable to deny this.
According to the right-wing commentator Nicholas Ebserstadt:

\begin{quote}
Stalin's results were incontestable. This is a point those of us in the
West often overlook. Stalin inherited a country that was the primary
casualty of World War I, and bequeathed to his successors a super-power.
It is but a single measure of the success of the `Leader', and his
understanding of the endurance of his nation, that between 1940 and
1953... the USSR doubled its production of coal and steel, tripled its
output of cement and industrial goods, and increased its pool of skilled
labor by a factor of ten\emph{.} These rates of growth were
geometrically higher than in the less devastated and Terror-free West.
\end{quote}

Now, let us discuss some of the particular issue relating to Stalin.

\subsection*{The Great Purge}

The purges of the late-1930's are a definite black mark on the legacy of
Soviet socialism; this much cannot be denied. That being said, they have
been the subject of decades-worth of unjustified and intolerable
distortions and exaggerations by bourgeois academics, necessitating a
thorough reply.

Firstly, let us establish the facts of how many people actually died in
the purges. While Westerners are often treated to numbers ranging from
20 to 50 million, the true figures (while bad enough in their own right)
are nowhere near that high. According to Professor
\href{https://history.ucla.edu/faculty/j-arch-getty}{J. Arch Getty}:

\begin{quote}
From 1921 to Stalin's death, in 1953, around 800,000 people were
sentenced to death and shot, 85 percent of them in the years of the
Great Terror of 1937-1938. From 1934 to Stalin's death, more than a
million perished in the gulag camps.
\end{quote}

To these figures must be added an important qualification: contrary to
popular opinion, the vast majority of gulag inmates were not innocent
political prisoners. Professor Getty notes that those convicted of
"counterrevolutionary crimes" made up between 12 and 33 percent
(depending on the year) of the gulag population, with the rest having
been convicted of ordinary crimes. He also rejects the common claim that
non-Russian nationalities were disproportionately targeted. To quote
from his article in the \emph{American Historical Review:}

\begin{quote}
The long-awaited archival evidence on repression in the period of the
Great Purges shows that the levels of arrests, political prisoners,
executions, and general camp populations tend to confirm the orders of
magnitude indicated by those labeled as "revisionists" and mocked by
those proposing high estimates... inferences that the terror fell
particularly hard on non-Russian nationalities are not borne out by the
camp population data from the 1930's. The frequent assertion that most
of the camp prisoners were 'political' also seems not to be true.
\end{quote}

These figures are confirmed in a
\href{https://www.cia.gov/library/readingroom/docs/CIA-RDP80T00246A032000400001-1.pdf}{CIA
report on the topic}.

In addition, the gulag camps were not death camps like those of the
Nazis; they were prisons, albeit harsh ones. Even noted anti-communist
scholars (such as those who worked on the infamous \emph{Black Book of
Communism}) have admitted this. To quote again from Professor Getty:

\begin{quote}
Stalin's camps were different from Hitler's. Tens of thousands of
prisoners were released every year upon completion of their sentences.
We now know that before World War II more inmates escaped annually from
the Soviet camps than died there. {[}...{]} Werth, a well-regarded
French specialist on the Soviet Union whose sections in the \emph{Black
Book} on the Soviet Communists are sober and damning, told \emph{Le
Monde,} "Death camps did not exist in the Soviet Union."
\end{quote}

It must also be noted that, contrary to the popular conception of
Stalin's USSR as a place of "total terror" (to quote Hannah Arendt), the
majority of the population did not feel threatened by the purges.
Referring to the time of the Great Purge, Professor
\href{http://miamioh.edu/cas/academics/departments/history/about/faculty/emeriti-faculty/thurston/index.html}{Robert
Thurston} notes that "my evidence suggests that widespread fear did not
exist in the case at hand." He also notes that the Great Purge was an
exceptional occurrence, which cannot be used to characterize the
Stalinist-era as a whole:

\begin{quote}
I will not simply imply but will state outright that the
\emph{Ezhovshchina} (Great Purge) was an aberration. Torture was
uncommon until August 1937, when it became the norm; it ended abruptly
with Beria's rise to head of the NKVD in late 1938. Mass arrests
followed the same pattern... A campaign for more regular, fair, and
systemic judicial procedures that began in 1933-1934 was interrupted and
overwhelmed by the Terror in 1937. It resumed in the spring of 1938,
more strongly and effectively than before. Thus more than one trend was
broken by the \emph{Ezhovshchina}, only to reappear after it.
\end{quote}

He also points out that some arrests which took place during the Great
Purge were based on previously-ignored (yet arguably still legitimate)
crimes against the Soviet state, such as fighting with the reactionary
forces during the Civil War:

\begin{quote}
People were suddenly arrested in 1937 for things that had happened many
years earlier but had been ignored since, for example, serving in a
White army.
\end{quote}

The question arises: why arrest former White Army soldiers, among
others? The answer lies in the general fear of counterrevolution which
pervaded the party at this time. According to Professor
\href{https://ahc.leeds.ac.uk/history/staff/64/professor-james-harris}{James
Harris}:

\begin{quote}
By the mid-1930's, the rise of the Nazis in Germany and the militarists
in Japan, both stridently anti-communist, posed a very real threat to
the USSR. War was then on the horizon, and Stalin felt he had no choice
but to take preemptive action against what he saw as a potential fifth
column -- a group that would undermine the larger collective.
\end{quote}

Remember that since the moment of its founding (still a recent event, at
this time), the Soviet Union had been
\href{https://www.warhistoryonline.com/world-war-i/the-day-that-the-usa-invaded-russia-and-fought-with-the-red-army-x.html}{invaded
by multiple capitalist powers} (including the United States) in the
early-1920's, and had also been subject to espionage and internal
sabotage. Combined with the looming threat of war with an increasingly
powerful Nazi Germany, it is hardly surprising that these factors came
together to form an atmosphere of paranoia, which lent itself to the
sort of violent excess seen during the Purge. This coincides with
Professor Thurston's interpretation of the events, from his book
\emph{Life and Terror in Stalin's Russia:}

\begin{quote}
...between 1934 and 1936 police and court practice relaxed
significantly. Then a series of events, together with the tense
international situation and memories of real enemy activity during the
savage Russian Civil War, combined to push leaders and people into a
hysterical hunt for perceived 'wreckers.' After late 1938, however, the
police and courts became dramatically milder.
\end{quote}

This general atmosphere of fear (not of the purges, but of external and
internal enemies) is most likely why the majority of the Soviet people
seemed to support the government's actions during the Purge period.
According to Professor Thurston:

\begin{quote}
The various reactions to arrest cataloged above suggest that general
fear did not exist in the USSR at any time in the late 1930's... People
who remained at liberty often felt that some event in the backgrounds of
the detained individuals justified their arrests. The sense that anyone
could be next, the underpinning of theoretical systems of terror, rarely
appears.
\end{quote}

Overall, perhaps the most succinct summary of this issue is the one
provided in Professor Thurston's book, in which he states:

\begin{quote}
There was never a long period of Stalinism without a serious foreign
threat, major internal dislocation, or both, which makes identifying its
true nature impossible.
\end{quote}

As Marxists, we should be well aware that material conditions shape
ideological and political structures. The savagery of the Russian Civil
War, the multiple invasions from capitalist powers, and the increasing
threat of a war against fascism make the paranoid atmosphere of the
late-1930's understandable, if not condonable; yet even while we discuss
the genuine causes of the Purge, and reject the hysterical
anti-communist mud-throwing of the Cold Warriors, we must still
acknowledge the black mark that the Purge leaves on Stalin's legacy.

\subsection*{The Ukrainian Famine}

Perhaps the most pernicious accusation against Stalin is that he
orchestrated the dreadful famine of the early-1930's in order to squash
a Ukrainian nationalist revolt. This despicable slander (which is
peddled largely by Ukrainian nationalist and neo-fascist groups) is
easily refuted by examining the historical consensus. The following
quotes are compiled in an article from the \emph{Village Voice}, cited
below.

\href{https://creees.stanford.edu/events/alexander-dallin-lecture-russian-east-european-and-eurasian-affairs}{Alexander
Dallin} of Stanford University writes:

\begin{quote}
There is no evidence it was intentionally directed against Ukrainians...
that would be totally out of keeping with what we know -\/- it makes no
sense.
\end{quote}

\href{https://www.upenn.edu/emeritus/memoriam/Lewin.html}{Moshe Lewin}
of the University of Pennsylvania stated:

\begin{quote}
This is crap, rubbish... I am an anti-Stalinist, but I don't see how
this {[}genocide{]} campaign adds to our knowledge. It's adding horrors,
adding horrors, until it becomes a pathology.
\end{quote}

\href{https://history.utoronto.ca/people/lynne-viola}{Lynne Viola} of
the University of Toronto writes:

\begin{quote}
I absolutely reject it... Why in god's name would this paranoid
government consciously produce a famine when they were terrified of war
{[}with Germany{]}?
\end{quote}

\href{https://history.wvu.edu/faculty-and-staff/faculty/mark-b-tauger}{Mark
Tauger}, Professor of History at West Virginia University (reviewing
work by
\href{https://pursuit.unimelb.edu.au/individuals/professor-stephen-wheatcroft}{Stephen
Wheatcroft} and
\href{https://www.birmingham.ac.uk/staff/profiles/gov/davies-bob.aspx}{R.W.
Davies}) has this to say:

\begin{quote}
Popular media and most historians for decades have described the great
famine that struck most of the USSR in the early 1930s as ``man-made,''
very often even a ``genocide'' that Stalin perpetrated intentionally
against Ukrainians and sometimes other national groups to destroy them
as nations... This perspective, however, is wrong. The famine that took
place was not limited to Ukraine or even to rural areas of the USSR, it
was not fundamentally or exclusively man-made, and it was far from the
intention of Stalin and others in the Soviet leadership to create such
as disaster. A small but growing literature relying on new archival
documents and a critical approach to other sources has shown the flaws
in the ``genocide'' or ``intentionalist'' interpretation of the famine
and has developed an alternative interpretation.
\end{quote}

More recent research has discovered natural causes for the Ukrainian
famine. Tauger notes:

\begin{quote}
...the USSR experienced an unusual environmental disaster in 1932:
extremely wet and humid weather that gave rise to severe plant disease
infestations, especially rust. Ukraine had double or triple the normal
rainfall in 1932. Both the weather conditions and the rust spread from
Eastern Europe, as plant pathologists at the time documented. Soviet
plant pathologists in particular estimated that rust and other fungal
diseases reduced the potential harvest in 1932 by almost nine million
tons, which is the largest documented harvest loss from any single cause
in Soviet history.
\end{quote}

It should be noted that this does not excuse the Soviet state from any
and all responsibility for the suffering that took place; one could
accuse the government of insufficiently rapid response, and note that
initial reports were often downplayed to avoid rocking the boat. But it
is clear that the famine was not deliberate, was not a genocide, and (to
quote Tauger) "was not fundamentally or exclusively man-made."

\subsection*{Conclusion}

During its socialist period, the Soviet Union made some of the most
impressive achievements in modern history. The socialist system
transformed a nation of illiterate and half-starved peasants into a
superpower, with one of the fastest growing economies on Earth, one of
the world's best-educated and healthiest populations, and some of the
most impressive industrial and technological achievements to date. It
provided a model for the oppressed people's of the world to follow, as
was shown in China, Cuba, Vietnam, and many other nations.

In part two, we will examine Soviet advances in women's rights and
anti-racism, as well as the Soviet role in global anti-imperialist
struggle. We will also examine the governance structure, learn about
Soviet democracy, and debunk the claim that the USSR was a "totalitarian
state".

\subsection*{Sources}

\begin{itemize}
\tightlist
\item
  \href{https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.507.8966\&rep=rep1\&type=pdf}{University
  of Oxford \textbar{} Farm to Factory: A Reassessment of the Soviet
  Industrial Revolution}
\item
  \href{https://web.williams.edu/Economics/brainerd/papers/ussr_july08.pdf}{Williams
  College \textbar{} Reassessing the Standard of Living in the Soviet
  Union: An Analysis Using Archival and Anthropometric Data}
\item
  \href{https://www.jstor.org/stable/25149595}{University of Warwick
  \textbar{} Review of "Farm to Factory"}
\item
  \href{https://academic.oup.com/ije/article/35/6/1384/660149}{Oxford
  University Press \textbar{} The Health Crisis in the USSR}
\item
  \href{https://link.springer.com/chapter/10.1007\%2F978-3-642-95874-8_22}{University
  of Munich \textbar{} Declining Life Expectancy in a Highly Developed
  Nation: Paradox or Statistical Artifact?}
\item
  \href{http://sovietinfo.tripod.com/WCR-German_Soviet.pdf}{University
  of Melbourne \textbar{} The Scale and Nature of German and Soviet Mass
  Killings and Repressions}
\item
  \href{http://sovietinfo.tripod.com/GTY-Penal_System.pdf}{American
  Historical Review \textbar{} Victims of the Soviet Penal System in the
  Pre-War Years}
\item
  \href{https://www.cia.gov/library/readingroom/docs/CIA-RDP80T00246A032000400001-1.pdf}{CIA
  (Freedom of Information Act) \textbar{} Report on Soviet Gulags}
\item
  \href{https://www.jstor.org/stable/j.ctt32bw0h}{Yale University Press
  \textbar{} Life and Terror in Stalin's Russia, 1934-1941}
\item
  \href{https://www.jstor.org/stable/2499177?seq=1\#metadata_info_tab_contents}{Slavic
  Review (Cambridge University Press) \textbar{} On Desk-Bound
  Parochialism, Commonsense Perspectives, and Lousy Evidence: A Response
  to Robert Conquest on the USSR}
\item
  \href{https://www.cambridge.org/core/journals/slavic-review/article/fear-and-belief-in-the-ussrs-great-terror-response-to-arrest-19351939/748662F55C0CCEE8096BDA2BB2CCCEE0}{Slavic
  Review \textbar{} Fear and Belief in the USSR's "Great Terror":
  Response to Arrest, 1935-1939}
\item
  \href{https://historynewsnetwork.org/article/163498}{History News
  Network \textbar{} Historian James Harris Says Russian Archives Show
  We've Misunderstood Stalin}
\item
  \href{https://msuweb.montclair.edu/~furrg/vv.html}{Village Voice
  \textbar{} In Search of a Soviet Holocaust: A Fifty Year-Old Famine
  Feeds the Right}
\item
  \href{https://eh.net/book_reviews/the-years-of-hunger-soviet-agriculture-1931-1933/}{EH
  Reviews \textbar{} The Years of Hunger: Soviet Agriculture, 1931-1933}
\end{itemize}
