\section{Workers' Control and Democracy in the Soviet Union}

\emph{\href{https://www.reddit.com/r/communism/comments/ehpdbp/masterpost_on_workers_control_and_democracy_in/}{Link
to post} by /u/flesh\_eating\_turtle}

\subsection*{Introduction}

One of the most common allegations leveled against the USSR (and
socialist states in general) by left-anticommunists is that it was not
"real socialism," because the workers did not have direct control over
production. This claim may be found in the writings of Noam Chomsky,
Murray Bookchin, Alexander Berkman, Emma Goldman, and numerous other
anti-Soviet leftists. It is claimed that the indisputable gains made by
the working class in socialist states (such as
\href{https://journals.sagepub.com/doi/abs/10.2190/B2TP-3R5M-Q7UP-DUA2}{vast
improvements to their health and welfare}) are irrelevant, because these
revolutions were "bureaucratic," and therefore, illegitimate.

The goal of this post is to demonstrate that, in point of fact, the
Soviet working class \emph{did} have a degree of workers' control, which
successfully gave Soviet workers far more rights and influence than
their capitalist counterparts.

As always, sources will be listed at the end.

\subsection*{Workers' Control in the Soviet Workplace}

When discussing this topic, it is helpful to start at the level of the
individual workplace. Professor
\href{http://miamioh.edu/cas/academics/departments/history/about/faculty/emeriti-faculty/thurston/index.html}{Robert
Thurston} (Miami University at Ohio) states that "at the lower levels of
society, in day-to-day affairs and the implementation of policy, {[}the
Soviet system{]} was participatory." He notes that workers were
frequently encouraged to take part in decision making:

\begin{quote}
The regime regularly urged its people to criticize local conditions and
their leaders, at least below a certain exalted level. For example, in
March 1937 Stalin emphasized the importance of the party's 'ties to the
masses'. To maintain them, it was necessary 'to listen carefully to the
voice of the masses, to the voice of rank and file members of the party,
to the voice of so-called "little people," to the voice of ordinary
folk.'
\end{quote}

These were not empty words or cheap propaganda; while there were limits
to criticism, Professor Thurston notes that "such bounds allowed a great
deal that was deeply significant to workers, including some aspects of
production norms, pay rates and classifications, safety on the job,
housing, and treatment by managers." The workers had a voice in various
official bodies, and they generally had their demands met:

\begin{quote}
The Commissariat of Justice also heard and responded to workers'
appeals. In August 1935 the Saratov city prosecutor reported that of 118
cases regarding pay recently handled by his office, 90, or 73.6 percent,
had been resolved in favor of workers.
\end{quote}

Workers also took part in direct oversight of managers:

\begin{quote}
Workers participated by the hundreds of thousands in special
inspectorates, commissions, and brigades which checked the work of
managers and institutions. These agencies sometimes wielded significant
power.
\end{quote}

The rights of Soviet workers were often noted in later accounts of the
socialist era:

\begin{quote}
One emigre recalled that his stepmother, a factory worker, 'often
scolded the boss,' and also complained about living conditions, but was
never arrested. John Scott, an American employed for years in the late
1930's as a welder in Magnitogorsk, attended a meeting at a Moscow
factory in 1940 where workers were able to 'criticize the plant
director, make suggestions as to how to increase production, increase
quality, and lower costs.'
\end{quote}

These facts are all the more impressive when we recall the dismal state
of workers' rights in the capitalist nations at this time:

\begin{quote}
This occurred at a time when American workers in particular were
struggling for basic union recognition, which even when won did not
provide much formal influence at the work place.
\end{quote}

Thurston makes the following observation:

\begin{quote}
Far from basing its rule on the negative means of coercion, the Soviet
regime in the late 1930's fostered a limited but positive political role
for the populace... Earlier concepts of the Soviet state require
rethinking: the workers who ousted managers, achieved the imprisonment
of their targets, and won reinstatement at factories did so through
organizations which constituted part of the state apparatus and wielded
state powers.
\end{quote}

He also notes that "no sharp division between state and society
existed," though different levels of the state wielded different powers.

In short, while the Soviet Union did have authoritarian elements (as was
inevitable given the conditions; the USSR had been ravaged by civil war,
and invaded by multiple capitalist nations), there was also a strong
element of workers' control, giving the USSR a legitimate claim to being
a workers' state.

\subsection*{Political Participation in the Soviet Union}

Working people did not only have the right to take part in
decision-making at the workplace; they also had a voice in national
policy decisions. Professor
\href{https://researchmap.jp/read0148263/?lang=english}{Kazuko Kawamoto}
(Hitotsubashi University) states that the USSR had "a more democratic
face than what is usually imagined, especially among Western people." As
they put it:

\begin{quote}
The Soviet regime was democratic in its own sense of the word...
participation through sending letters and attending discussions gave
self-government a certain reality and helped to legitimize the Soviet
regime. Therefore, listening to the people was an important obligation
for the authorities... the government encouraged people to send letters
to the authorities and actively used the all-people's discussions.
\end{quote}

These all-people's discussions existed from the early days of the Soviet
Union, and they had great significance (contrary to the assumptions of
Western scholars):

\begin{quote}
Although the first all-people's discussion was conducted with the
approval of the 1936 Stalin constitution on the grounds that the former
ruling classes no longer existed, publication and public discussion of
bills had been common before the constitution in the name of
participation of the masses. Western scholars usually took this as an
attempt to put a face of legitimacy on the process, understanding the
discussions to be a mere formality. However, that is not the case with
the Principles argued here. The discussions were neither a disguise nor
a mere formality.
\end{quote}

Legislators took direct part in these meetings, altering proposed bills
in accordance with popular opinion. Professor Kawamoto states that " it
is worth pointing out that members of the subcommittee actively
participated in the discussion, rewriting the draft at the same time."

It is also noted that Soviet citizens "believed that they were entitled
to demand policy changes, and the draft writers, including specialists,
officials, and deputies, felt obliged to respond to those demands." The
process of gathering public opinion was intensive enough that it often
slowed down the process of legislation:

\begin{quote}
Regarding the process of creating the Principles, direct participation
worked largely as expected in the ideology of Soviet democracy, although
it took many years.
\end{quote}

As Professor Kawamoto says, "the reason why it took so long was deeply
rooted in the ideas of Soviet democracy." Contrast this with bourgeois
democracy, where
\href{https://scholar.princeton.edu/sites/default/files/mgilens/files/gilens_and_page_2014_-testing_theories_of_american_politics.doc.pdf}{legislators
typically disregard the opinion of the masses}. This may speed up the
legislative process, but it results in
\href{https://www.washingtonpost.com/news/rampage/wp/2015/09/23/americans-have-grown-to-really-really-hate-their-government/}{extremely
high levels of popular discontent}.

In addition to the aforementioned means of popular participation, Soviet
officials also traveled throughout the nation to gather information on
popular opinion. Using the development of Soviet family law as an
example, Professor Kawamoto states:

\begin{quote}
The draft makers were not only passive recipients of letters but also
traveled throughout the Soviet Union to listen to the people. When the
work in the Commissions of Legislative Proposals was reaching its end,
members of the subcommittee and officials working for them visited
several union republics from April to June 1962 to research the practice
of family law and collect opinions on important standards in the draft
of the Principles... After these research trips, the commission finished
the draft and presented it to the Central Committee of the Party in
July.
\end{quote}

While Soviet democracy was not without its flaws (as mentioned, the
process was often rather slow, and there were limits to the extent of
criticism), it would be highly inaccurate to describe the USSR as a
"totalitarian" society, with no democratic structures; on the contrary,
the USSR did practice its own form of democracy, and it did so rather
effectively.

\subsection*{Conclusion}

The Soviet Union developed under conditions of extreme pressure, facing
\href{https://en.wikipedia.org/wiki/American_Expeditionary_Force,_Siberia}{invasion
from capitalist powers}, {[}the Nazi
invasion{]}(\url{https://en.wikipedia.org/wiki/Eastern_Front_(World_War_II)}
(the deadliest front in military history), and espionage from the West.
Given the difficulties that it faced, it is remarkable the USSR managed
to provide a positive political role for the working people, especially
in a time when workers in the capitalist world were still struggling for
basic union rights.

The USSR was a legitimate workers' state, in which the proletariat held
power in the workplace, and had significant influence on national policy
decisions. Contrast this with the utter lack of popular influence in
bourgeois states, and this is even easier to appreciate.

\subsection*{Sources}

\begin{itemize}
\tightlist
\item
  \href{https://www.docdroid.net/t9gG4jQ/thurston-robert-reassessing-the-history-of-soviet-workers-opportunities-to-criticize-and-participate-in-decision-making.pdf}{International
  Council for Central and East European Studies \textbar{} Reassessing
  the History of Soviet Workers: Opportunities to Criticize and
  Participate in Decision-Making, 1935-1941}
\item
  \href{http://jpsa-web.org/eibun_zassi/data/pdf/JPSA_Kawamoto_final_July_9_2014.pdf}{Japanese
  Political Science Review \textbar{} Rethinking Soviet Democracy}
\item
  \href{https://scholar.princeton.edu/sites/default/files/mgilens/files/gilens_and_page_2014_-testing_theories_of_american_politics.doc.pdf}{Princeton
  University \textbar{} Testing Theories of American Politics}
\item
  \href{https://www.washingtonpost.com/news/rampage/wp/2015/09/23/americans-have-grown-to-really-really-hate-their-government/}{Washington
  Post \textbar{} Americans Have Grown to Really, Really Hate Their
  Government}
\item
  \href{https://en.wikipedia.org/wiki/American_Expeditionary_Force,_Siberia}{Wikipedia
  \textbar{} American Expeditionary Force, Siberia}
\item
  \href{https://en.wikipedia.org/wiki/Eastern_Front_(World_War_II)}{Wikipedia
  \textbar{} Eastern Front (World War II)}
\end{itemize}
