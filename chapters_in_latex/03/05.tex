\section{Stalin and the Great Purge}

\emph{\href{https://www.reddit.com/r/communism/comments/e8kpow/masterpost_on_joseph_stalin_and_the_great_purge/}{Link
to post} by /u/flesh\_eating\_turtle}

\emph{"Contradiction is universal and absolute, it is present in the
process of development of all things and permeates every process from
beginning to end... the history of a society is the history of its
internal contradictions." - Mao Zedong}

\subsection*{Introduction}

Hello comrades. Normally our masterposts deal with entire nations or
recent events, but seeing as this particular topic is one of the most
discussed in online political circles, it seems deserving of its own
discussion. This is especially true because this topic often acts as a
barrier to those who wish to learn more about communism, and
Marxism-Leninism in particular.

In order to deal with this topic, I decided to put together a discussion
of Joseph Stalin from a Marxist-Leninist perspective. In it, we will
deal with Stalin's successes and failures, correct various
misconceptions and falsehoods surrounding him, and place the man in his
proper historical context.

It should also be said, this discussion will not gloss over and/or
ignore Stalin's various flaws and misdoings; "Stalin did nothing wrong"
is a good meme, but it's a bad analysis.

As always, all sources will be listed at the end of the post.

\subsection*{The Soviet Economy Under Stalin}

It is commonly alleged that Stalin presided over a period of economic
failure in the USSR, due to his insistence upon industrialization and
the collectivization of agriculture. However, more recent research has
painted a far more positive picture.

According to Professor
\href{https://scholar.google.ae/citations?user=o-1ldbkAAAAJ\&hl=en}{Robert
Allen}:

\begin{quote}
The Soviet economy performed well... Planning led to high rates of
capital accumulation, rapid GDP growth, and rising per capita
consumption even in the 1930's. {[}...{]} The expansion of heavy
industry and the use of output targets and soft-budgets to direct firms
were appropriate to the conditions of the 1930's, they were adopted
quickly, and they led to rapid growth of investment and consumption.
\end{quote}

Professor \href{http://www.ebrainerd.com/}{Elizabeth Brainerd} refers to
Soviet growth rates as "impressive," noting that they "promoted the
rapid industrialization of the USSR, particularly in the decades from
the 1930's to the 1960's." She also states:

\begin{quote}
Both Western and Soviet estimates of GNP growth in the Soviet Union
indicate that GNP per capita grew in every decade in the postwar era, at
times far surpassing the growth rates of the developed western
economies.
\end{quote}

Even still, it is often claimed that this growth did not improve the
standard of living for the Soviet people. However, more recent research
has also shown this to be false.

According to Professor Allen:

\begin{quote}
While investment certainly increased rapidly, recent research shows that
the standard of living also increased briskly. {[}...{]} Calories are
the most basic dimension of the standard of living, and their
consumption was higher in the late 1930's than in the 1920's. {[}...{]}
There has been no debate that `collective consumption' (principally
education and health services) rose sharply, but the standard view was
that private consumption declined. Recent research, however, calls that
conclusion into question... Consumption per head rose about one quarter
between 1928 and the late 1930's.
\end{quote}

According to Professor Brainerd:

\begin{quote}
The conventional measures of GNP growth and household consumption
indicate a long, uninterrupted upward climb in the Soviet standard of
living from 1928 to 1985; even Western estimates of these measures
support this view, albeit at a slower rate of growth than the Soviet
measures.
\end{quote}

While the economy is not the main purpose of this discussion, it should
be acknowledged that under Joseph Stalin, the Soviet Union experienced
rapid economic growth, and a significant increase in the population's
standard of living.

\subsection*{The Great Purge}

The purges of the late-1930's are a definite black mark on the legacy of
Soviet socialism; this much cannot be denied. That being said, they have
been the subject of decades-worth of unjustified and intolerable
distortions and exaggerations by bourgeois academics, necessitating a
thorough reply.

Firstly, let us establish the facts of how many people actually died in
the purges. While Westerners are often treated to numbers ranging from
20 to 50 million, the true figures (while bad enough in their own right)
are nowhere near that high. According to Professor
\href{https://history.ucla.edu/faculty/j-arch-getty}{J. Arch Getty}:

\begin{quote}
From 1921 to Stalin's death, in 1953, around 800,000 people were
sentenced to death and shot, 85 percent of them in the years of the
Great Terror of 1937-1938. From 1934 to Stalin's death, more than a
million perished in the gulag camps.
\end{quote}

To these figures must be added an important qualification: contrary to
popular opinion, the vast majority of gulag inmates were not innocent
political prisoners. Professor Getty notes that those convicted of
"counterrevolutionary crimes" made up between 12 and 33 percent
(depending on the year) of the gulag population, with the rest having
been convicted of ordinary crimes. He also rejects the common claim that
non-Russian nationalities were disproportionately targeted. To quote
from his article in the \emph{American Historical Review:}

\begin{quote}
The long-awaited archival evidence on repression in the period of the
Great Purges shows that the levels of arrests, political prisoners,
executions, and general camp populations tend to confirm the orders of
magnitude indicated by those labeled as "revisionists" and mocked by
those proposing high estimates... inferences that the terror fell
particularly hard on non-Russian nationalities are not borne out by the
camp population data from the 1930's. The frequent assertion that most
of the camp prisoners were 'political' also seems not to be true.
\end{quote}

In addition, the gulags were not death camps like those of the Nazis;
they were prisons, albeit harsh ones. Even noted anti-communist scholars
(such as those who worked on the infamous \emph{Black Book of
Communism}) have admitted this. To quote again from Professor Getty:

\begin{quote}
Stalin's camps were different from Hitler's. Tens of thousands of
prisoners were released every year upon completion of their sentences.
We now know that before World War II more inmates escaped annually from
the Soviet camps than died there. {[}...{]} Werth, a well-regarded
French specialist on the Soviet Union whose sections in the \emph{Black
Book} on the Soviet Communists are sober and damning, told \emph{Le
Monde,} "Death camps did not exist in the Soviet Union."
\end{quote}

It must also be noted that, contrary to the popular conception of
Stalin's USSR as a place of "total terror" (to quote Hannah Arendt), the
majority of the population did not feel threatened by the purges.
Referring to the time of the Great Purge, Professor
\href{http://miamioh.edu/cas/academics/departments/history/about/faculty/emeriti-faculty/thurston/index.html}{Robert
Thurston} notes that "my evidence suggests that widespread fear did not
exist in the case at hand." He also notes that the Great Purge was an
exceptional occurrence, which cannot be used to characterize the
Stalinist-era as a whole:

\begin{quote}
I will not simply imply but will state outright that the
\emph{Ezhovshchina} {[}Great Purge{]} was an aberration. Torture was
uncommon until August 1937, when it became the norm; it ended abruptly
with Beria's rise to head of the NKVD in late 1938. Mass arrests
followed the same pattern... A campaign for more regular, fair, and
systemic judicial procedures that began in 1933-1934 was interrupted and
overwhelmed by the Terror in 1937. It resumed in the spring of 1938,
more strongly and effectively than before. Thus more than one trend was
broken by the \emph{Ezhovshchina}, only to reappear after it.
\end{quote}

He also points out that some arrests which took place during the Great
Purge were based on previously-ignored (yet arguably still legitimate)
crimes against the Soviet state, such as fighting with the reactionary
forces during the Civil War:

\begin{quote}
People were suddenly arrested in 1937 for things that had happened many
years earlier but had been ignored since, for example, serving in a
White army.
\end{quote}

The question arises: why arrest former White Army soldiers, among
others? The answer lies in the general fear of counterrevolution which
pervaded the party at this time. According to Professor
\href{https://ahc.leeds.ac.uk/history/staff/64/professor-james-harris}{James
Harris}:

\begin{quote}
By the mid-1930's, the rise of the Nazis in Germany and the militarists
in Japan, both stridently anti-communist, posed a very real threat to
the USSR. War was then on the horizon, and Stalin felt he had no choice
but to take preemptive action against what he saw as a potential fifth
column -- a group that would undermine the larger collective.
\end{quote}

Remember that since the moment of its founding (still a recent event, at
this time), the Soviet Union had been
\href{https://www.warhistoryonline.com/world-war-i/the-day-that-the-usa-invaded-russia-and-fought-with-the-red-army-x.html}{invaded
by multiple capitalist powers} (including the United States) in the
early-1920's, and had also been subject to espionage and internal
sabotage. Combined with the looming threat of war with an increasingly
powerful Nazi Germany, it is hardly surprising that these factors came
together to form an atmosphere of paranoia, which lent itself to the
sort of violent excess seen during the Purge. This coincides with
Professor Thurston's interpretation of the events, from his book
\emph{Life and Terror in Stalin's Russia:}

\begin{quote}
...between 1934 and 1936 police and court practice relaxed
significantly. Then a series of events, together with the tense
international situation and memories of real enemy activity during the
savage Russian Civil War, combined to push leaders and people into a
hysterical hunt for perceived 'wreckers.' After late 1938, however, the
police and courts became dramatically milder.
\end{quote}

This general atmosphere of fear (not of the purges, but of external and
internal enemies) is most likely why the majority of the Soviet people
seemed to support the government's actions during the Purge period.
According to Professor Thurston:

\begin{quote}
The various reactions to arrest cataloged above suggest that general
fear did not exist in the USSR at any time in the late 1930's... People
who remained at liberty often felt that some event in the backgrounds of
the detained individuals justified their arrests. The sense that anyone
could be next, the underpinning of theoretical systems of terror, rarely
appears.
\end{quote}

Overall, perhaps the most succinct summary of this issue is the one
provided in Professor Thurston's book, in which he states:

\begin{quote}
There was never a long period of Stalinism without a serious foreign
threat, major internal dislocation, or both, which makes identifying its
true nature impossible.
\end{quote}

As Marxists, we should be well aware that material conditions shape
ideological and political structures. The savagery of the Russian Civil
War, the multiple invasions from capitalist powers, and the increasing
threat of a war against fascism make the paranoid atmosphere of the
late-1930's understandable, if not condonable; yet even while we discuss
the genuine causes of the Purge, and reject the hysterical
anti-communist mud-throwing of the Cold Warriors, we must still
acknowledge the black mark that the Purge leaves on Stalin's legacy.

\subsection*{The Ukrainian Famine ("Holodomor")}

Perhaps the most pernicious accusation against Stalin is that he
orchestrated the dreadful famine of the early-1930's in order to squash
a Ukrainian nationalist revolt. This despicable slander (which is
peddled largely by Ukrainian nationalist and neo-fascist groups) is
easily refuted by examining the historical consensus. The following
quotes are compiled in an article from the \emph{Village Voice}, cited
below.

\href{https://creees.stanford.edu/events/alexander-dallin-lecture-russian-east-european-and-eurasian-affairs}{Alexander
Dallin} of Stanford University writes:

\begin{quote}
There is no evidence it was intentionally directed against Ukrainians...
that would be totally out of keeping with what we know -\/- it makes no
sense.
\end{quote}

\href{https://www.upenn.edu/emeritus/memoriam/Lewin.html}{Moshe Lewin}
of the University of Pennsylvania stated:

\begin{quote}
This is crap, rubbish... I am an anti-Stalinist, but I don't see how
this {[}genocide{]} campaign adds to our knowledge. It's adding horrors,
adding horrors, until it becomes a pathology.
\end{quote}

\href{https://history.utoronto.ca/people/lynne-viola}{Lynne Viola} of
the University of Toronto writes:

\begin{quote}
I absolutely reject it... Why in god's name would this paranoid
government consciously produce a famine when they were terrified of war
{[}with Germany{]}?
\end{quote}

\href{https://history.wvu.edu/faculty-and-staff/faculty/mark-b-tauger}{Mark
Tauger}, Professor of History at West Virginia University (reviewing
work by
\href{https://pursuit.unimelb.edu.au/individuals/professor-stephen-wheatcroft}{Stephen
Wheatcroft} and
\href{https://www.birmingham.ac.uk/staff/profiles/gov/davies-bob.aspx}{R.W.
Davies}) has this to say:

\begin{quote}
Popular media and most historians for decades have described the great
famine that struck most of the USSR in the early 1930s as ``man-made,''
very often even a ``genocide'' that Stalin perpetrated intentionally
against Ukrainians and sometimes other national groups to destroy them
as nations... This perspective, however, is wrong. The famine that took
place was not limited to Ukraine or even to rural areas of the USSR, it
was not fundamentally or exclusively man-made, and it was far from the
intention of Stalin and others in the Soviet leadership to create such
as disaster. A small but growing literature relying on new archival
documents and a critical approach to other sources has shown the flaws
in the ``genocide'' or ``intentionalist'' interpretation of the famine
and has developed an alternative interpretation.
\end{quote}

More recent research has discovered natural causes for the Ukrainian
famine. Tauger notes:

\begin{quote}
...the USSR experienced an unusual environmental disaster in 1932:
extremely wet and humid weather that gave rise to severe plant disease
infestations, especially rust. Ukraine had double or triple the normal
rainfall in 1932. Both the weather conditions and the rust spread from
Eastern Europe, as plant pathologists at the time documented. Soviet
plant pathologists in particular estimated that rust and other fungal
diseases reduced the potential harvest in 1932 by almost nine million
tons, which is the largest documented harvest loss from any single cause
in Soviet history.
\end{quote}

It should be noted that this does not excuse the Soviet state from any
and all responsibility for the suffering that took place; one could
accuse the government of insufficiently rapid response, and note that
initial reports were often downplayed to avoid rocking the boat. But it
is clear that the famine was not deliberate, was not a genocide, and (to
quote Tauger) "was not fundamentally or exclusively man-made."

\subsection*{Conclusion}

In short, comrades, Stalin was an extraordinarily complex man. His
legacy is far too nuanced to be summed-up in catchphrases like "Stalin
was worse than Hitler," or "Stalin did nothing wrong." He was a flawed
leader, who managed some enormous achievements (rapid industrial and
economic growth, improvements to the standard of living, leading the
USSR to victory over Nazism), while also making some enormous mistakes
(the paranoia of the purges, the errors of the famine, the rolling-back
of progressive social policies).

We should not respond to bourgeois propaganda by insisting (as some
well-meaning yet mistaken comrades have done) that every single misdeed
of Stalin is a lie; rather, we should place them into proper historical
context, along with his various achievements. This is the correct way
for Marxists to analyze the world: with a firm, well-grounded
materialist critique, yielding no ground to hero worship, or to a
fictitious "great man" theory of history. Recall what Fidel Castro said
on the matter:

\begin{quote}
I believe Stalin made big mistakes but also showed great wisdom. In my
opinion, blaming Stalin for everything that occurred in the Soviet Union
would be historical simplism, because no man by himself could have
created certain conditions. It would be the same as giving Stalin all
the credit for what the USSR once was. That is impossible! I believe
that the efforts of millions and millions of heroic people contributed
to the USSR's development and to its relevant role in the world in favor
of hundreds of millions of people. {[}...{]} I think there should be an
impartial analysis of Stalin. Blaming him for everything that happened
would be historical simplism.
\end{quote}

Stalin was a great proletarian revolutionary, and yet he was also a
highly flawed leader. Do not be ashamed of this legacy, comrades;
rather, recall that in all things, there is contradiction.

\subsection*{Sources}

\begin{itemize}
\tightlist
\item
  \href{https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.507.8966\&rep=rep1\&type=pdf}{University
  of Oxford \textbar{} A Reassessment of the Soviet Industrial
  Revolution}
\item
  \href{https://web.williams.edu/Economics/brainerd/papers/ussr_july08.pdf}{Williams
  College \textbar{} Reassessing the Standard of Living in the Soviet
  Union: An Analysis Using Archival and Anthropometric Data}
\item
  \href{https://www.theatlantic.com/magazine/archive/2000/03/the-future-did-not-work/378081/}{The
  Atlantic \textbar{} The Future Did Not Work}
\item
  \href{http://sovietinfo.tripod.com/GTY-Penal_System.pdf}{American
  Historical Review \textbar{} Victims of the Soviet Penal System in the
  Pre-War Years: A First Approach on the Basis of Archival Evidence}
\item
  \href{https://www.jstor.org/stable/j.ctt32bw0h}{Yale University Press
  \textbar{} Life and Terror in Stalin's Russia, 1934-1941}
\item
  \href{https://www.jstor.org/stable/2499177?seq=1}{Slavic Review
  \textbar{} On Desk-Bound Parochialism, Commonsense Perspectives, and
  Lousy Evidence: A Reply to Robert Conquest}
\item
  \href{https://www.cambridge.org/core/journals/slavic-review/article/fear-and-belief-in-the-ussrs-great-terror-response-to-arrest-19351939/748662F55C0CCEE8096BDA2BB2CCCEE0}{Slavic
  Review \textbar{} Fear and Belief in the USSR's "Great Terror":
  Response to Arrest, 1935-1939}
\item
  \href{https://historynewsnetwork.org/article/163498}{History News
  Network \textbar{} Historian James Harris Says Russian Archives Show
  We've Misunderstood Stalin}
\item
  \href{https://msuweb.montclair.edu/~furrg/vv.html}{Village Voice
  \textbar{} In Search of a Soviet Holocaust: A Fifty Year-Old Famine
  Feeds the Right}
\item
  \href{https://eh.net/book_reviews/the-years-of-hunger-soviet-agriculture-1931-1933/}{EH
  Reviews \textbar{} The Years of Hunger: Soviet Agriculture, 1931-1933}
\item
  \href{https://www.marxists.org/history/cuba/archive/castro/1992/06/03.htm}{Marxists
  Internet Archive \textbar{} Fidel Castro on Stalin}
\end{itemize}
